/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#include <gtest/gtest.h>

#include <json-rpc/client.h>
#include <json-rpc/server.h>


namespace {

struct echo_method
{
  std::string m_expected;
  size_t      m_invoked = 0;
  size_t      m_succeeded = 0;

  inline echo_method(std::string const & expected)
    : m_expected{expected}
  {
  }


  inline std::optional<jsonrpc::response>
  operator()(jsonrpc::request const & req)
  {
    ++m_invoked;
    EXPECT_EQ(m_expected, req.params["message"].get<std::string>());

    jsonrpc::json result = R"({})"_json;
    result["message"] = req.params["message"];
    jsonrpc::response resp{req.id, result};

    ++m_succeeded;
    return resp;
  }
};

} // anonymous namespace


TEST(Comms, echo)
{
  using namespace jsonrpc;

  // Need server and client that cease to exist at the end of the test, but
  // we need the variable names early - unique_ptr it is.
  std::unique_ptr<client> cl;
  std::unique_ptr<server> srv;

  // Need I/O functions for client and server - they each write to the other's
  // consume functio and that's it.
  auto client_io = [&srv](io::peer_tag const &, char const * buf, size_t size)
  {
    srv->consume("client", buf, size);
  };

  auto server_io = [&cl](io::peer_tag const &, char const * buf, size_t size)
  {
    cl->consume("server", buf, size);
  };

  // Create client and server.
  cl = std::make_unique<client>(client_io);
  srv = std::make_unique<server>(server_io);

  // Register an echo method
  echo_method echo{"Hello, comms!"};
  srv->register_method("echo", std::ref(echo));

  // Send a request. We use a request specific callback to ensure we're
  // getting our response.
  size_t response_received = 0;
  cl->send_request("server", "echo",
      R"({"message":"Hello, comms!"})"_json,
      [&response_received](io::peer_tag const &, result_state state, std::optional<response> response)
      {
        ASSERT_EQ(RE_OK_RESPONSE, state);
        ASSERT_TRUE(response.has_value());

        auto & resp = response.value();
        ASSERT_EQ(std::string{"Hello, comms!"},
            resp.result["message"].get<std::string>());

        ++response_received;
      }
  );

  ASSERT_EQ(echo.m_invoked, 1);
  ASSERT_EQ(echo.m_succeeded, 1);
  ASSERT_EQ(response_received, 1);
}
