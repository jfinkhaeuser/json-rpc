/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#include <gtest/gtest.h>

#include <json-rpc/request.h>


TEST(Request, construct_with_array_params)
{
  jsonrpc::request req{R"({
    "jsonrpc": "2.0",
    "method": "foo",
    "params": [42, 128],
    "id": 123
  })"_json};

  ASSERT_TRUE(req.is_valid());
  ASSERT_FALSE(req.is_internal());

  ASSERT_TRUE(req.is_request());
  ASSERT_FALSE(req.is_notification());

  ASSERT_TRUE(req.has_params());
}


TEST(Request, construct_with_object_params)
{
  jsonrpc::request req{R"({
    "jsonrpc": "2.0",
    "method": "foo",
    "params": {"x": 42},
    "id": 123
  })"_json};

  ASSERT_TRUE(req.is_valid());
  ASSERT_FALSE(req.is_internal());

  ASSERT_TRUE(req.is_request());
  ASSERT_FALSE(req.is_notification());

  ASSERT_TRUE(req.has_params());
}


TEST(Request, construct_without_params)
{
  jsonrpc::request req{R"({
    "jsonrpc": "2.0",
    "method": "foo",
    "id": 123
  })"_json};

  ASSERT_TRUE(req.is_valid());
  ASSERT_FALSE(req.is_internal());

  ASSERT_TRUE(req.is_request());
  ASSERT_FALSE(req.is_notification());

  ASSERT_FALSE(req.has_params());
}


TEST(Request, construct_with_null_params)
{
  jsonrpc::request req{R"({
    "jsonrpc": "2.0",
    "method": "foo",
    "params": null,
    "id": 123
  })"_json};

  ASSERT_FALSE(req.is_valid());
}


TEST(Request, construct_as_notification)
{
  jsonrpc::request req{R"({
    "jsonrpc": "2.0",
    "method": "foo",
    "params": [42, 128]
  })"_json};

  ASSERT_TRUE(req.is_valid());
  ASSERT_FALSE(req.is_internal());

  ASSERT_FALSE(req.is_request());
  ASSERT_TRUE(req.is_notification());

  ASSERT_TRUE(req.has_params());
}


TEST(Request, construct_as_internal)
{
  jsonrpc::request req{R"({
    "jsonrpc": "2.0",
    "method": "rpc.foo",
    "params": [42, 128],
    "id": 123
  })"_json};

  ASSERT_TRUE(req.is_valid());
  ASSERT_TRUE(req.is_internal());

  ASSERT_TRUE(req.is_request());
  ASSERT_FALSE(req.is_notification());

  ASSERT_TRUE(req.has_params());
}


TEST(Request, construct_bad_jsonrpc)
{
  jsonrpc::request req{R"({
    "jsonrpc": "1.0",
    "method": "foo",
    "params": [42, 128],
    "id": 123
  })"_json};

  ASSERT_FALSE(req.is_valid());
}


TEST(Request, construct_missing_jsonrpc)
{
  jsonrpc::request req{R"({
    "method": "foo",
    "params": [42, 128],
    "id": 123
  })"_json};

  ASSERT_FALSE(req.is_valid());
}


TEST(Request, construct_bad_method_empty)
{
  jsonrpc::request req{R"({
    "jsonrpc": "2.0",
    "method": "",
    "params": [42, 128],
    "id": 123
  })"_json};

  ASSERT_FALSE(req.is_valid());
}


TEST(Request, construct_bad_method_number)
{
  jsonrpc::request req{R"({
    "jsonrpc": "2.0",
    "method": 123,
    "params": [42, 128],
    "id": 123
  })"_json};

  ASSERT_FALSE(req.is_valid());
}



TEST(Request, construct_missing_method)
{
  jsonrpc::request req{R"({
    "jsonrpc": "2.0",
    "params": [42, 128],
    "id": 123
  })"_json};

  ASSERT_FALSE(req.is_valid());
}


TEST(Request, construct_bad_id)
{
  jsonrpc::request req{R"({
    "jsonrpc": "2.0",
    "method": 123,
    "params": [42, 128],
    "id": []
  })"_json};

  ASSERT_FALSE(req.is_valid());
}
