/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#include <gtest/gtest.h>

#include <json-rpc/processing/client.h>
#include <json-rpc/processing/server.h>


TEST(ProcessingComms, single_method)
{
  jsonrpc::processing::server srv;

  // Register a single method
  auto res = srv.register_method("foo", [](jsonrpc::request const & req) -> jsonrpc::response
  {
    jsonrpc::json j;
    j = "Hello, " + req.params[0].get<std::string>() + "!";
    return {req.id, j};
  });
  ASSERT_TRUE(res);

  // Now invoke the method with some request.
  jsonrpc::processing::client client;
  auto req = client.create_request("foo", R"(["world"])"_json);
  ASSERT_TRUE(client.is_in_flight(req.id));

  auto resp_opt = srv.invoke(req);

  ASSERT_TRUE(resp_opt.has_value());
  auto resp = resp_opt.value();

  ASSERT_TRUE(resp.is_valid());
  ASSERT_FALSE(resp.is_error());

  // Process response
  auto results = client.receive_response(resp);
  ASSERT_EQ(1, results.size());
  jsonrpc::result_state result;
  std::tie(result, std::ignore, std::ignore) = results[0];
  ASSERT_EQ(jsonrpc::RE_OK_RESPONSE, result);
  ASSERT_FALSE(client.is_in_flight(req.id));
}


TEST(ProcessingComms, batch)
{
  jsonrpc::processing::server srv;

  // Register a single method
  auto res = srv.register_method("foo", [](jsonrpc::request const & req) -> jsonrpc::response
  {
    jsonrpc::json j;
    j = "Hello, " + req.params[0].get<std::string>() + "!";
    return {req.id, j};
  });
  ASSERT_TRUE(res);

  // Now invoke the method with several requests
  jsonrpc::processing::client client;
  auto requests = client.batch()
    .create_request("foo", R"(["world"])"_json)
    .create_request("foo", R"(["whatevs"])"_json)
    .get();
  ASSERT_EQ(2, client.num_in_flight());

  auto results = srv.invoke(requests);
  ASSERT_EQ(2, results.size());

  // Process the result list
  auto processed = client.receive_responses(results);
  ASSERT_EQ(2, processed.size());
  ASSERT_EQ(0, client.num_in_flight());

  jsonrpc::result_state p;
  for (auto & entry : processed) {
    std::tie(p, std::ignore, std::ignore) = entry;
    ASSERT_EQ(jsonrpc::RE_OK_RESPONSE, p);
  }
}
