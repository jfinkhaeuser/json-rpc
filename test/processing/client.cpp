/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#include <gtest/gtest.h>

#include <json-rpc/processing/client.h>

TEST(ProcessingClient, simple_method_flow)
{
  jsonrpc::processing::client client;
  ASSERT_EQ(0, client.num_in_flight());

  auto req = client.create_request("foo", R"([0, 1])"_json);
  ASSERT_TRUE(req.is_valid());

  ASSERT_EQ("foo", req.method);
  ASSERT_EQ(R"([0,1] )"_json, req.params);

  ASSERT_EQ(1, client.num_in_flight());
  ASSERT_TRUE(client.is_in_flight(req.id));
  ASSERT_FALSE(client.is_in_flight(req.id.get<int>() + 1));

  // Create response, and feed it to the client. Doesn't matter what is in the
  // response, so we don't add anything.
  jsonrpc::response resp{req.id, "null"_json};
  auto results = client.receive_response(resp);
  ASSERT_EQ(1, results.size());
  jsonrpc::result_state res;
  std::tie(res, std::ignore, std::ignore) = results[0];
  ASSERT_EQ(jsonrpc::RE_OK_RESPONSE, res);

  ASSERT_EQ(0, client.num_in_flight());
  ASSERT_FALSE(client.is_in_flight(req.id));
}


TEST(ProcessingClient, invalid_response)
{
  jsonrpc::processing::client client;
  ASSERT_EQ(0, client.num_in_flight());

  auto req = client.create_request("foo", R"([0, 1])"_json);
  ASSERT_TRUE(req.is_valid());

  ASSERT_EQ("foo", req.method);
  ASSERT_EQ(R"([0,1] )"_json, req.params);

  ASSERT_EQ(1, client.num_in_flight());
  ASSERT_TRUE(client.is_in_flight(req.id));
  ASSERT_FALSE(client.is_in_flight(req.id.get<int>() + 1));

  // Create response; make it invalid by not feeding it a result.
  jsonrpc::response resp{req.id};
  auto results = client.receive_response(resp);
  ASSERT_EQ(1, results.size());
  jsonrpc::result_state res;
  std::tie(res, std::ignore, std::ignore) = results[0];
  ASSERT_EQ(jsonrpc::RE_PROCESSING_ERROR, res);
}


TEST(ProcessingClient, response_with_bad_id)
{
  jsonrpc::processing::client client;
  ASSERT_EQ(0, client.num_in_flight());

  auto req = client.create_request("foo", R"([0, 1])"_json);
  ASSERT_TRUE(req.is_valid());

  ASSERT_EQ("foo", req.method);
  ASSERT_EQ(R"([0,1] )"_json, req.params);

  ASSERT_EQ(1, client.num_in_flight());
  ASSERT_TRUE(client.is_in_flight(req.id));
  ASSERT_FALSE(client.is_in_flight(req.id.get<int>() + 1));

  // Create response; make it invalid by not feeding it a result.
  jsonrpc::response resp{req.id.get<std::size_t>() + 1, "null"_json};
  auto results = client.receive_response(resp);
  ASSERT_EQ(1, results.size());
  jsonrpc::result_state res;
  std::tie(res, std::ignore, std::ignore) = results[0];
  ASSERT_EQ(jsonrpc::RE_UNKNOWN_REQUEST_ID, res);

  ASSERT_EQ(1, client.num_in_flight());
  ASSERT_TRUE(client.is_in_flight(req.id));
}


TEST(ProcessingClient, response_with_error)
{
  jsonrpc::processing::client client;
  ASSERT_EQ(0, client.num_in_flight());

  auto req = client.create_request("foo", R"([0, 1])"_json);
  ASSERT_TRUE(req.is_valid());

  ASSERT_EQ("foo", req.method);
  ASSERT_EQ(R"([0,1] )"_json, req.params);

  ASSERT_EQ(1, client.num_in_flight());
  ASSERT_TRUE(client.is_in_flight(req.id));
  ASSERT_FALSE(client.is_in_flight(req.id.get<int>() + 1));

  // Create response with error
  jsonrpc::response resp{req.id, jsonrpc::error{jsonrpc::ERR_PARSE_ERROR, "blah"}};
  auto results = client.receive_response(resp);
  ASSERT_EQ(1, results.size());
  jsonrpc::result_state res;
  std::tie(res, std::ignore, std::ignore) = results[0];
  ASSERT_EQ(jsonrpc::RE_ERROR_RESPONSE, res);

  ASSERT_EQ(0, client.num_in_flight());
  ASSERT_FALSE(client.is_in_flight(req.id));
}



TEST(ProcessingClient, notification)
{
  jsonrpc::processing::client client;
  ASSERT_EQ(0, client.num_in_flight());

  auto req = client.create_notification("foo", R"([0, 1])"_json);
  ASSERT_TRUE(req.is_valid());
  ASSERT_TRUE(req.is_notification());

  ASSERT_EQ("foo", req.method);
  ASSERT_EQ(R"([0,1] )"_json, req.params);

  ASSERT_EQ(0, client.num_in_flight());
  ASSERT_FALSE(client.is_in_flight(req.id));
}


TEST(ProcessingClient, batch)
{
  {
    jsonrpc::processing::client client;
    ASSERT_EQ(0, client.num_in_flight());

    auto builder = client.batch();

    builder.create_request("foo", R"([])"_json);
    ASSERT_EQ(1, client.num_in_flight());

    builder.create_notification("bar", R"([])"_json);
    ASSERT_EQ(1, client.num_in_flight());

    builder.create_request("baz", R"([])"_json);
    ASSERT_EQ(2, client.num_in_flight());

    // Two requests in-flight, but our result is three sized
    auto res = builder.get();
    ASSERT_EQ(3, res.size());
  }

  // Same, but with the syntax convenience the builder offers
  {
    jsonrpc::processing::client client;
    ASSERT_EQ(0, client.num_in_flight());

    auto res = client.batch()
      .create_request("foo", R"([])"_json)
      .create_notification("bar", R"([])"_json)
      .create_request("baz", R"([])"_json)
      .get();

    ASSERT_EQ(2, client.num_in_flight());
    ASSERT_EQ(3, res.size());
  }
}


TEST(ProcessingClient, simple_method_timeout)
{
  // We need to create the client with a timeout, and pass the
  // relative_time_point to various functions.
  using namespace std::literals::chrono_literals;
  constexpr const jsonrpc::duration TIMEOUT = 7ms;

  jsonrpc::relative_time_point now = 1ms;

  jsonrpc::processing::client client{TIMEOUT};
  ASSERT_EQ(0, client.num_in_flight());

  auto req = client.create_request("foo", R"([0, 1])"_json, now);
  ASSERT_TRUE(req.is_valid());

  ASSERT_EQ("foo", req.method);
  ASSERT_EQ(R"([0,1] )"_json, req.params);

  ASSERT_EQ(1, client.num_in_flight());
  ASSERT_TRUE(client.is_in_flight(req.id));
  ASSERT_FALSE(client.is_in_flight(req.id.get<int>() + 1));

  // If we increment now to the timeout, then processing a
  // response should fail with a timeout.
  now += TIMEOUT;

  jsonrpc::response resp{req.id, "null"_json};
  auto results = client.receive_response(resp, now);
  ASSERT_EQ(1, results.size());
  jsonrpc::result_state res;
  std::tie(res, std::ignore, std::ignore) = results[0];
  ASSERT_EQ(jsonrpc::RE_TIMEOUT, res);

  ASSERT_EQ(0, client.num_in_flight());
  ASSERT_FALSE(client.is_in_flight(req.id));
}
