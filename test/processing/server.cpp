/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#include <gtest/gtest.h>

#include <json-rpc/processing/server.h>


TEST(ProcessingServer, single_method)
{
  jsonrpc::processing::server srv;

  // Register a single method
  auto res = srv.register_method("foo", [](jsonrpc::request const & req) -> jsonrpc::response
  {
    jsonrpc::json j;
    j = "Hello, " + req.params[0].get<std::string>() + "!";
    return {req.id, j};
  });
  ASSERT_TRUE(res);

  // Now invoke the method with some request.
  jsonrpc::request req{R"({
    "jsonrpc": "2.0",
    "method": "foo",
    "id": 42,
    "params": ["world"]
  })"_json};

  auto resp_opt = srv.invoke(req);

  ASSERT_TRUE(resp_opt.has_value());
  auto resp = resp_opt.value();

  ASSERT_TRUE(resp.is_valid());
  ASSERT_FALSE(resp.is_error());

  ASSERT_EQ(req.id, resp.id);
  ASSERT_EQ("Hello, world!", resp.result.get<std::string>());
}


TEST(ProcessingServer, single_method_bad_param)
{
  jsonrpc::processing::server srv;

  // Register a single method
  auto res = srv.register_method("foo", [](jsonrpc::request const & req) -> jsonrpc::response
  {
    jsonrpc::json j;
    j = "Hello, " + req.params[0].get<std::string>() + "!";
    return {req.id, j};
  });
  ASSERT_TRUE(res);

  // Now invoke the method with some request.
  jsonrpc::request req{R"({
    "jsonrpc": "2.0",
    "method": "foo",
    "id": 42,
    "params": {}
  })"_json};

  auto resp_opt = srv.invoke(req);

  ASSERT_TRUE(resp_opt.has_value());
  auto resp = resp_opt.value();

  ASSERT_TRUE(resp.is_valid());
  ASSERT_TRUE(resp.is_error());

  ASSERT_EQ(req.id, resp.id);

  ASSERT_EQ(jsonrpc::ERR_INTERNAL, resp.error.code);
}


TEST(ProcessingServer, single_method_explicit_error)
{
  jsonrpc::processing::server srv;

  // Register a single method
  auto res = srv.register_method("foo", [](jsonrpc::request const &) -> jsonrpc::response
  {
    throw jsonrpc::error{jsonrpc::ERR_PARSE_ERROR, "test"};
  });
  ASSERT_TRUE(res);

  // Now invoke the method with some request.
  jsonrpc::request req{R"({
    "jsonrpc": "2.0",
    "method": "foo",
    "id": 42,
    "params": {}
  })"_json};

  auto resp_opt = srv.invoke(req);

  ASSERT_TRUE(resp_opt.has_value());
  auto resp = resp_opt.value();

  ASSERT_TRUE(resp.is_valid());
  ASSERT_TRUE(resp.is_error());

  ASSERT_EQ(req.id, resp.id);

  ASSERT_EQ(jsonrpc::ERR_PARSE_ERROR, resp.error.code);
  ASSERT_EQ("test", resp.error.message);
}


TEST(ProcessingServer, single_notification)
{
  // Processing notifications is the same as processing methods, but no response
  // should be returned.

  jsonrpc::processing::server srv;

  // Register a single method
  auto res = srv.register_method("foo", [](jsonrpc::request const &) -> std::optional<jsonrpc::response>
  {
    // Do nothing
    return std::nullopt;
  });
  ASSERT_TRUE(res);

  // Now invoke the method with some request.
  jsonrpc::request req{R"({
    "jsonrpc": "2.0",
    "method": "foo",
    "params": ["world"]
  })"_json};

  auto resp_opt = srv.invoke(req);

  // If the result has no value, it also means there was no error.
  ASSERT_FALSE(resp_opt.has_value());
}


TEST(ProcessingServer, batch)
{
  jsonrpc::processing::server srv;
  auto res = srv.register_method("foo", [](jsonrpc::request const & req) -> jsonrpc::response
  {
    // Do nothing
    return {req.id, "[]"_json};
  });
  ASSERT_TRUE(res);

  res = srv.register_method("err", [](jsonrpc::request const &) -> jsonrpc::response
  {
    throw std::runtime_error("nope");
  });
  ASSERT_TRUE(res);

  res = srv.register_method("notify", [](jsonrpc::request const &) -> std::optional<jsonrpc::response>
  {
    return std::nullopt;
  });
  ASSERT_TRUE(res);


  // Create batch
  jsonrpc::processing::server::request_list list;

  // First a regular request
  list.push_back(jsonrpc::request{R"({
    "jsonrpc": "2.0",
    "method": "foo",
    "params": ["world"],
    "id": 42
  })"_json});

  // A notification
  list.push_back(jsonrpc::request{R"({
    "jsonrpc": "2.0",
    "method": "notify"
  })"_json});

  // Something that produces an error
  list.push_back(jsonrpc::request{R"({
    "jsonrpc": "2.0",
    "method": "err",
    "id": 123
  })"_json});


  // Invoke
  auto results = srv.invoke(list);

  // Since the notification should not produce a result, our results
  // must have 2 entries.
  ASSERT_EQ(2, results.size());

  // The first result must be for ID 42 and not be an error (all we care about)
  auto resp = results[0];
  ASSERT_TRUE(resp.is_valid());
  ASSERT_EQ(42, resp.id);
  ASSERT_FALSE(resp.is_error());

  // The second result must be for ID 123 and an error (all we care about)
  resp = results[1];
  ASSERT_TRUE(resp.is_valid());
  ASSERT_EQ(123, resp.id);
  ASSERT_TRUE(resp.is_error());
}
