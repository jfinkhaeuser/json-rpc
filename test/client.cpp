/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#include <gtest/gtest.h>

#include <sstream>

#include <json-rpc/client.h>
#include <json-rpc/response.h>

#include "../lib/parse/incremental_parser.h"

namespace {

struct echo_parsing
{
  size_t          m_write_invoked = 0;
  size_t          m_succeeded = 0;
  uint64_t        m_id = 0;

  std::string     m_expected_tag;
  size_t          m_expected_min_size;

  jsonrpc::client m_client;

  inline echo_parsing(std::string const & expected_tag,
      size_t expected_min_size)
    : m_expected_tag{expected_tag}
    , m_expected_min_size{expected_min_size}
    , m_client{[this](jsonrpc::io::peer_tag const & tag,
        char const * buf, size_t size) -> void
        {
          ++m_write_invoked;
          ASSERT_EQ(m_expected_tag, tag);
          ASSERT_GE(size, m_expected_min_size);

          // Parse ID from request here, so we can create a response.
          jsonrpc::parse::incremental_parser<char> parser{
            [this](jsonrpc::json const & value)
            {
              m_id = value["id"].get<uint64_t>();
              ++m_succeeded;
            }
          };
          parser.push(buf, buf + size);
        }
      }
  {
    m_client.on_log_output([](std::string const & msg)
        {
          std::cerr << "LOG: " << msg << std::endl;
        });
  }
};


struct test_callback
{
  size_t                            m_invoked = 0;
  size_t                            m_succeeded = 0;
  std::optional<jsonrpc::response>  m_response = {};

  jsonrpc::io::peer_tag             m_expected_tag;
  jsonrpc::result_state             m_expected_state;

  inline test_callback(jsonrpc::io::peer_tag const & tag,
      jsonrpc::result_state state)
    : m_expected_tag{tag}
    , m_expected_state{state}
  {
  }

  inline void
  operator()(jsonrpc::io::peer_tag const & tag,
      jsonrpc::result_state state,
      std::optional<jsonrpc::response> response)
  {
    ++m_invoked;
    ASSERT_EQ(m_expected_tag, tag);
    ASSERT_EQ(m_expected_state, state);
    m_response = response;
    ++m_succeeded;
  }
};


} // anonymous namespace

TEST(Client, write_callback)
{
  using namespace jsonrpc;

  size_t write_invoked = 0;
  std::string io_tag;
  client cl{[&write_invoked, &io_tag](io::peer_tag const & tag, char const *, size_t size) -> void
    {
      ++write_invoked;
      ASSERT_EQ("server", tag);
      ASSERT_GT(size, 20); // "message" + "Hello, world!" + ...
      io_tag = tag;
    }
  };

  cl.send_request("server", "echo", R"({ "message": "Hello, world!" })"_json);

  ASSERT_EQ(write_invoked, 1);
  ASSERT_EQ("server", io_tag);
}


TEST(Client, response_generic_callback)
{
  // Send request and parse ID sent
  echo_parsing ep{"server", 20};
  ep.m_client.send_request("server", "echo", R"({ "message": "Hello, world!" })"_json);

  ASSERT_EQ(ep.m_write_invoked, 1);
  ASSERT_EQ(ep.m_succeeded, 1);
  ASSERT_NE(ep.m_id, 0);

  // Register callbacks
  test_callback generic_cb{"server", jsonrpc::RE_OK_RESPONSE};
  ep.m_client.on_response(std::ref(generic_cb));

  // Craft a response.
  jsonrpc::response resp{ep.m_id, R"({ "message": "Hello, world!" })"_json};
  std::stringstream sstream;
  sstream << resp;
  std::string buf = sstream.str();

  // Consume response
  ep.m_client.consume("server", buf.c_str(), buf.size());

  // Check the callback was invoked correctly.
  ASSERT_EQ(1, generic_cb.m_invoked);
  ASSERT_EQ(1, generic_cb.m_succeeded);
}



TEST(Client, response_tag_callback)
{
  // Send request and parse ID sent
  echo_parsing ep{"server", 20};
  ep.m_client.send_request("server", "echo", R"({ "message": "Hello, world!" })"_json);

  ASSERT_EQ(ep.m_write_invoked, 1);
  ASSERT_EQ(ep.m_succeeded, 1);
  ASSERT_NE(ep.m_id, 0);

  // Register callbacks
  test_callback generic_cb{"server", jsonrpc::RE_OK_RESPONSE};
  ep.m_client.on_response(std::ref(generic_cb));

  test_callback good_tag_cb{"server", jsonrpc::RE_OK_RESPONSE};
  ep.m_client.on_response("server", std::ref(good_tag_cb));

  test_callback bad_tag_cb{"fnargh", jsonrpc::RE_OK_RESPONSE};
  ep.m_client.on_response("fnargh", std::ref(bad_tag_cb));

  // Craft a response.
  jsonrpc::response resp{ep.m_id, R"({ "message": "Hello, world!" })"_json};
  std::stringstream sstream;
  sstream << resp;
  std::string buf = sstream.str();

  // Consume response
  ep.m_client.consume("server", buf.c_str(), buf.size());

  // Check the callback was invoked correctly.
  ASSERT_EQ(0, generic_cb.m_invoked);
  ASSERT_EQ(0, generic_cb.m_succeeded);

  ASSERT_EQ(1, good_tag_cb.m_invoked);
  ASSERT_EQ(1, good_tag_cb.m_succeeded);

  ASSERT_EQ(0, bad_tag_cb.m_invoked);
  ASSERT_EQ(0, bad_tag_cb.m_succeeded);
}



TEST(Client, response_request_callback)
{
  // Send request and parse ID sent
  test_callback request_cb{"server", jsonrpc::RE_OK_RESPONSE};

  echo_parsing ep{"server", 20};
  ep.m_client.send_request("server", "echo", R"({ "message": "Hello, world!" })"_json,
      std::ref(request_cb));

  ASSERT_EQ(ep.m_write_invoked, 1);
  ASSERT_EQ(ep.m_succeeded, 1);
  ASSERT_NE(ep.m_id, 0);

  // Register callbacks
  test_callback generic_cb{"server", jsonrpc::RE_OK_RESPONSE};
  ep.m_client.on_response(std::ref(generic_cb));

  test_callback good_tag_cb{"server", jsonrpc::RE_OK_RESPONSE};
  ep.m_client.on_response("server", std::ref(good_tag_cb));

  test_callback bad_tag_cb{"fnargh", jsonrpc::RE_OK_RESPONSE};
  ep.m_client.on_response("fnargh", std::ref(bad_tag_cb));

  // Craft a response.
  jsonrpc::response resp{ep.m_id, R"({ "message": "Hello, world!" })"_json};
  std::stringstream sstream;
  sstream << resp;
  std::string buf = sstream.str();

  // Consume response
  ep.m_client.consume("server", buf.c_str(), buf.size());

  // Check the callback was invoked correctly.
  ASSERT_EQ(0, generic_cb.m_invoked);
  ASSERT_EQ(0, generic_cb.m_succeeded);

  ASSERT_EQ(0, good_tag_cb.m_invoked);
  ASSERT_EQ(0, good_tag_cb.m_succeeded);

  ASSERT_EQ(0, bad_tag_cb.m_invoked);
  ASSERT_EQ(0, bad_tag_cb.m_succeeded);

  ASSERT_EQ(1, request_cb.m_invoked);
  ASSERT_EQ(1, request_cb.m_succeeded);
}



TEST(Client, timeout)
{
  using namespace jsonrpc;
  using namespace std::literals::chrono_literals;

  // Need to create relative time points
  constexpr duration const TIMEOUT = 10ms;
  relative_time_point now = 1ms;

  // Client; the write callback is barely necessary.
  size_t write_invoked = 0;
  client cl{[&write_invoked](io::peer_tag const &, char const *, size_t) -> void
    {
      ++write_invoked;
    },
    TIMEOUT
  };

  // Send request and parse ID sent
  cl.send_request("server", "echo", R"({ "message": "Hello, world!" })"_json,
      {}, // No request-specific callback
      now);

  ASSERT_EQ(write_invoked, 1);

  // Register callbacks
  size_t response_invoked = 0;
  cl.on_response([&response_invoked](io::peer_tag const &, result_state state, std::optional<response>)
      {
        ASSERT_EQ(RE_TIMEOUT, state);
        ++response_invoked;
      });

  // Progress clock and process timeouts (we don't need to consume here)
  now += TIMEOUT;
  cl.process_timeouts(now);

  ASSERT_EQ(response_invoked, 1);
}
