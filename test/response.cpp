/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#include <gtest/gtest.h>

#include <json-rpc/response.h>


TEST(Response, with_null_result)
{
  jsonrpc::response res{R"({
    "jsonrpc": "2.0",
    "id": 123,
    "result": null
  })"_json};

  ASSERT_TRUE(res.is_valid());
  ASSERT_FALSE(res.is_error());
}


TEST(Response, with_result)
{
  jsonrpc::response res{R"({
    "jsonrpc": "2.0",
    "id": 123,
    "result": [1, 2]
  })"_json};

  ASSERT_TRUE(res.is_valid());
  ASSERT_FALSE(res.is_error());

  ASSERT_TRUE(res.result.is_array());
}


TEST(Response, without_result)
{
  jsonrpc::response res{R"({
    "jsonrpc": "2.0",
    "id": 123
  })"_json};

  ASSERT_FALSE(res.is_valid());
}


TEST(Response, with_error)
{
  jsonrpc::response res{R"({
    "jsonrpc": "2.0",
    "id": 123,
    "error": {
      "code": 1234,
      "message": "not good"
    }
  })"_json};

  ASSERT_TRUE(res.is_valid());
  ASSERT_TRUE(res.is_error());

  ASSERT_EQ(1234, res.error.code);
  ASSERT_EQ("not good", res.error.message);
}


TEST(Response, with_bad_error)
{
  jsonrpc::response res{R"({
    "jsonrpc": "2.0",
    "id": 123,
    "error": {
    }
  })"_json};

  ASSERT_FALSE(res.is_valid());
}


TEST(Response, malformed)
{
  jsonrpc::response res{R"({
    "jsonrpc": "2.0"
  })"_json};

  ASSERT_FALSE(res.is_valid());
}
