/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#include <gtest/gtest.h>

#include <nlohmann/json.hpp>

#include "../lib/parse/partial_buffer_container.h"


template <typename T>
class PartialBufferContainer: public ::testing::Test {};
TYPED_TEST_SUITE_P(PartialBufferContainer);


TYPED_TEST_P(PartialBufferContainer, simple)
{
  auto text = "test string";
  TypeParam cont{text, text + std::strlen(text)};

  // Just make sure we can iterate over the entire string
  ASSERT_EQ(11, cont.size());

  std::size_t items = 0;
  for (auto c [[maybe_unused]] : cont) {
    ++items;
  }

  ASSERT_EQ(cont.size(), items);
}


TYPED_TEST_P(PartialBufferContainer, simple_no_init)
{
  auto text = "test string";
  TypeParam cont;
  ASSERT_EQ(0, cont.size());

  std::size_t items = 0;
  for (auto c [[maybe_unused]] : cont) {
    ++items;
  }
  ASSERT_EQ(cont.size(), items);


  // Set the input
  cont.set_input(text, text + std::strlen(text));
  ASSERT_EQ(11, cont.size());

  items = 0;
  for (auto c [[maybe_unused]] : cont) {
    ++items;
  }

  ASSERT_EQ(cont.size(), items);
}





TYPED_TEST_P(PartialBufferContainer, with_remains)
{
  auto text = "first:second";
  TypeParam cont{text, text + std::strlen(text)};

  // The full length is static
  ASSERT_EQ(12, cont.size());

  // Iterate to the colon, and consider this part done
  auto iter = cont.begin();
  while (*iter != ':') { ++iter; }
  auto diff = iter - cont.begin();
  ASSERT_EQ(5, diff);

  // Copy the remaining values (minus the colon itself, it's supposed to be our
  // separator in this test)
  auto copied = cont.copy_remaining(++iter);
  ASSERT_EQ(6, copied);
  ASSERT_EQ(6, cont.size());

  // The input function provides a new input buffer to the container
  auto next = "-something!";
  cont.set_input(next, next + std::strlen(next));

  ASSERT_EQ(17, cont.size());

  // New iterator; should point to an 's' from "second"
  iter = cont.begin();
  ASSERT_EQ('s', *iter);

  // Incrementing the iterator 6 times should give us the beginning '-' of the
  // new input
  ++iter; ++iter; ++iter;
  ++iter; ++iter; ++iter;
  ASSERT_EQ('-', *iter);
}



TYPED_TEST_P(PartialBufferContainer, copy_too_much_failure)
{
  auto text = "first:second";
  TypeParam cont{text, text + std::strlen(text)};

  // The full length is static
  ASSERT_EQ(12, cont.size());

  ASSERT_THROW(cont.copy_remaining(13), std::runtime_error);
}


TYPED_TEST_P(PartialBufferContainer, remains_copy_from_buffer)
{
  auto text = "first:second";
  TypeParam cont{text, text + std::strlen(text)};

  // The full length is static
  ASSERT_EQ(12, cont.size());

  // Copy a part
  auto copied = cont.copy_remaining(6);
  ASSERT_EQ(6, copied);
  ASSERT_EQ(6, cont.size());

  // Provide new input
  auto next = "-something!";
  cont.set_input(next, next + std::strlen(next));

  // Now copy everything from the index 3; this is still in the buffer copied
  // before, but we'll also expect all the new input.
  copied = cont.copy_remaining(4);
  ASSERT_EQ(13, copied);
  ASSERT_EQ(13, cont.size());

  // An iterator should point at the 'n' in "second"
  auto iter = cont.begin();
  ASSERT_EQ('n', *iter);

  // Incrementing the iterator a few times should stay in the now-copied buffer.
  ++iter; ++iter;
  ASSERT_EQ('-', *iter);
}


TYPED_TEST_P(PartialBufferContainer, copy_remaining_partial_input)
{
  auto text = "first";
  TypeParam cont{text, text + std::strlen(text)};
  ASSERT_EQ(5, cont.size());

  text = ":second";
  cont.continue_with(0, text, text + std::strlen(text));
  ASSERT_EQ(12, cont.size());

  // At this point, the first five characters are in the buffer, the remaining 7
  // in the input.
  cont.copy_remaining(6);
  ASSERT_EQ(6, cont.size());
  auto iter = cont.begin();
  ASSERT_EQ('s', *iter);
}


TYPED_TEST_P(PartialBufferContainer, incremental_io)
{
  auto text = "first:second";
  TypeParam cont{text, text + std::strlen(text)};

  // The full length is static
  ASSERT_EQ(12, cont.size());

  // The continue_with() function takes an offset and a new input, and essentially
  // subsumes the functionality from copy_remaining() and set_input(). It also
  // returns a new begin() iterator.
  auto iter = cont.begin();
  ++iter; ++iter; ++iter;
  ++iter; ++iter;
  ASSERT_EQ(':', *iter);
  ++iter;

  auto next = "-something";
  iter = cont.continue_with(iter, next, next + std::strlen(next));
  ASSERT_EQ('s', *iter);

  ++iter; ++iter; ++iter;
  ++iter; ++iter; ++iter;
  ASSERT_EQ('-', *iter);
}


TYPED_TEST_P(PartialBufferContainer, truncate_front_input_only)
{
  auto text = "first:second";
  TypeParam cont{text, text + std::strlen(text)};

  // The full length is static
  ASSERT_EQ(12, cont.size());

  // Truncate the first five characters, so we should start with a colon
  // afterwards.
  cont.truncate_front(5);
  ASSERT_EQ(7, cont.size());
  auto iter = cont.begin();
  ASSERT_EQ(':', *iter);
}


TYPED_TEST_P(PartialBufferContainer, truncate_whole_buffer)
{
  auto text = "first";
  TypeParam cont{text, text + std::strlen(text)};
  ASSERT_EQ(5, cont.size());

  text = ":second";
  cont.continue_with(0, text, text + std::strlen(text));
  ASSERT_EQ(12, cont.size());

  // At this point, the first five characters are in the buffer, the remaining 7
  // in the input.
  cont.truncate_front(5);
  ASSERT_EQ(7, cont.size());
  auto iter = cont.begin();
  ASSERT_EQ(':', *iter);
}


TYPED_TEST_P(PartialBufferContainer, truncate_partial_buffer)
{
  auto text = "first";
  TypeParam cont{text, text + std::strlen(text)};
  ASSERT_EQ(5, cont.size());

  text = ":second";
  cont.continue_with(0, text, text + std::strlen(text));
  ASSERT_EQ(12, cont.size());

  // At this point, the first five characters are in the buffer, the remaining 7
  // in the input.
  cont.truncate_front(3);
  ASSERT_EQ(9, cont.size());
  auto iter = cont.begin();
  ASSERT_EQ('s', *iter);
}


TYPED_TEST_P(PartialBufferContainer, truncate_everything)
{
  auto text = "first";
  TypeParam cont{text, text + std::strlen(text)};
  ASSERT_EQ(5, cont.size());

  text = ":second";
  cont.continue_with(0, text, text + std::strlen(text));
  ASSERT_EQ(12, cont.size());

  // At this point, the first five characters are in the buffer, the remaining 7
  // in the input.
  cont.truncate_front(cont.max_offset());
  ASSERT_EQ(0, cont.size());
}


TYPED_TEST_P(PartialBufferContainer, truncate_everything_with_copy_remaining)
{
  auto text = "first";
  TypeParam cont{text, text + std::strlen(text)};
  ASSERT_EQ(5, cont.size());

  text = ":second";
  cont.continue_with(0, text, text + std::strlen(text));
  ASSERT_EQ(12, cont.size());

  // At this point, the first five characters are in the buffer, the remaining 7
  // in the input.
  cont.copy_remaining(cont.max_offset());
  ASSERT_EQ(0, cont.size());
}


TYPED_TEST_P(PartialBufferContainer, as_json_input)
{
  // This test predominantly must compile
  using json = nlohmann::json;

  auto text = R"({"foo": 42})";
  TypeParam cont{text, text + std::strlen(text)};

  auto res = json::parse(cont);

  ASSERT_TRUE(res.is_object());
  ASSERT_TRUE(res.contains("foo"));

  // XXX: For some reason .get<int>() does not compile; luckily, there's a
  //      way to circumvent.
  int val = 0;
  res["foo"].get_to(val);
  ASSERT_EQ(42, val);
}



REGISTER_TYPED_TEST_SUITE_P(PartialBufferContainer,
    simple,
    simple_no_init,
    with_remains,
    copy_too_much_failure,
    remains_copy_from_buffer,
    copy_remaining_partial_input,
    incremental_io,
    truncate_front_input_only,
    truncate_whole_buffer,
    truncate_partial_buffer,
    truncate_everything,
    truncate_everything_with_copy_remaining,
    as_json_input
);

typedef ::testing::Types<
  jsonrpc::parse::partial_buffer_container<
    char,
    jsonrpc::parse::detail::array_buffer<char, 1024>
  >,
  jsonrpc::parse::partial_buffer_container<
    char,
    jsonrpc::parse::detail::vector_buffer<char>
  >
> test_types;
INSTANTIATE_TYPED_TEST_SUITE_P(parse, PartialBufferContainer, test_types);



TEST(ParsePartialBufferContainerRestricted, remains_copy_as_truncate)
{
  auto text = "first:second";
  using pbc = jsonrpc::parse::partial_buffer_container<char, jsonrpc::parse::detail::array_buffer<char, 3>>;
  pbc cont{text, text + std::strlen(text)};

  // The full length is static
  ASSERT_EQ(12, cont.size());

  // Copying nothing should succeed but not change anything.
  auto copied = cont.copy_remaining(cont.size());
  ASSERT_EQ(0, copied);
  ASSERT_EQ(0, cont.size());
}


TEST(ParsePartialBufferContainerRestricted, remains_copy_with_restricted_buffer)
{
  auto text = "first:second";
  using pbc = jsonrpc::parse::partial_buffer_container<char, jsonrpc::parse::detail::array_buffer<char, 3>>;
  pbc cont{text, text + std::strlen(text)};

  // The full length is static
  ASSERT_EQ(12, cont.size());

  // Copying the entire buffer must fail.
  ASSERT_THROW(cont.copy_remaining(0), std::runtime_error);

  // But also copying a sufficiently small number of items should. Let's take
  // all that fit, 3
  auto copied = cont.copy_remaining(cont.size() - 3);
  ASSERT_EQ(3, copied);
  ASSERT_EQ(3, cont.size());

  auto iter = cont.begin();
  ASSERT_EQ('o', *iter);
}



