/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#include <gtest/gtest.h>

#include "../lib/parse/incremental_parser.h"


TEST(ParseIncrementalParser, parse_simple_object)
{
  // A single input makes things easiest.
  using namespace jsonrpc::parse;
  using parser_type = incremental_parser<char>;

  std::size_t called = 0;
  parser_type parser{[&called](jsonrpc::json const & value)
    {
      ++called;
      ASSERT_TRUE(value.is_object());
      ASSERT_TRUE(value.contains("foo"));
      ASSERT_EQ(value["foo"].get<std::string>(), "bar");
    }
  };

  auto text = R"({"foo":"bar"})";
  parser.push(text, text + std::strlen(text));

  ASSERT_EQ(1, called);
}


TEST(ParseIncrementalParser, parse_invalid_tokens)
{
  // A single input makes things easiest.
  using namespace jsonrpc::parse;
  using parser_type = incremental_parser<char>;

  std::size_t called = 0;
  parser_type parser{[&called](jsonrpc::json const & value)
    {
      ++called;
      ASSERT_TRUE(value.is_object());
      ASSERT_TRUE(value.contains("foo"));
      ASSERT_EQ(value["foo"].get<std::string>(), "bar");
    }
  };

  auto text = R"({invalid_tokens {"foo":"bar"})";
  parser.push(text, text + std::strlen(text));

  ASSERT_EQ(1, called);
}


TEST(ParseIncrementalParser, parse_simple_array)
{
  // A single input makes things easiest.
  using namespace jsonrpc::parse;
  using parser_type = incremental_parser<char>;

  std::size_t called = 0;
  parser_type parser{[&called](jsonrpc::json const & value)
    {
      ++called;
      ASSERT_TRUE(value.is_array());
      ASSERT_EQ(3, value.size());
    }
  };

  auto text = R"([0, 1, 2])";
  parser.push(text, text + std::strlen(text));

  ASSERT_EQ(1, called);
}



TEST(ParseIncrementalParser, parse_multiple)
{
  using namespace jsonrpc::parse;
  using parser_type = incremental_parser<char>;

  std::size_t called = 0;
  std::set<jsonrpc::json> values;
  parser_type parser{[&called, &values](jsonrpc::json const & value)
    {
      values.insert(value);
      ++called;
    }
  };

  // Things get more interesting with a sequence of inputs
  auto text = R"([0, 1, 2]{"foo":"bar"}{"baz":"quux"})";
  parser.push(text, text + std::strlen(text));

  ASSERT_EQ(3, called);
  ASSERT_EQ(1, values.count(R"([0,1,2])"_json));
  ASSERT_EQ(1, values.count(R"({"foo": "bar" })"_json));
  ASSERT_EQ(1, values.count(R"({ "baz" : "quux" })"_json));
}


TEST(ParseIncrementalParser, parse_partial)
{
  using namespace jsonrpc::parse;
  using parser_type = incremental_parser<char>;

  std::size_t called = 0;
  std::set<jsonrpc::json> values;
  parser_type parser{[&called, &values](jsonrpc::json const & value)
    {
      values.insert(value);
      ++called;
    }
  };

  // Parse a partial buffer
  auto text = R"([0, 1, 2]{"foo":)";
  parser.push(text, text + std::strlen(text));

  ASSERT_EQ(1, called);
  ASSERT_EQ(1, values.count(R"([0,1,2])"_json));

  // If we push the rest of the string, that should yield the same values
  // as in our previous test.
  text = R"("bar"}{"baz":"quux"})";
  parser.push(text, text + std::strlen(text));

  ASSERT_EQ(3, called);
  ASSERT_EQ(1, values.count(R"({"foo": "bar" })"_json));
  ASSERT_EQ(1, values.count(R"({ "baz" : "quux" })"_json));
}


TEST(ParseIncrementalParser, parse_partial_reused_buffer)
{
  using namespace jsonrpc::parse;
  using parser_type = incremental_parser<char>;

  std::size_t called = 0;
  std::set<jsonrpc::json> values;
  parser_type parser{[&called, &values](jsonrpc::json const & value)
    {
      values.insert(value);
      ++called;
    }
  };

  // Parse a partial buffer
  char buf[100] = R"({"foo":)";
  parser.push(buf, buf + std::strlen(buf));

  ASSERT_EQ(0, called);

  // Reuse the buffer (override existing) and push it to parser
  strcpy(buf, R"("bar"}{"baz":"quux"})");
  parser.push(buf, buf + std::strlen(buf));

  ASSERT_EQ(2, called);
  ASSERT_EQ(1, values.count(R"({"foo": "bar" })"_json));
  ASSERT_EQ(1, values.count(R"({ "baz" : "quux" })"_json));
}


TEST(ParseIncrementalParser, parse_garbage)
{
  using namespace jsonrpc::parse;
  using parser_type = incremental_parser<char>;

  std::size_t called = 0;
  std::set<jsonrpc::json> values;
  parser_type parser{[&called, &values](jsonrpc::json const & value)
    {
      values.insert(value);
      ++called;
    }
  };

  // Parse a partial buffer
  auto text = R"([0, 1, 2]{"foo":)";
  parser.push(text, text + std::strlen(text));

  ASSERT_EQ(1, called);
  ASSERT_EQ(1, values.count(R"([0,1,2])"_json));

  // If we next push gargage, we want to detect that.
  text = "-somegarbage";
  ASSERT_THROW(parser.push(text, text + std::strlen(text)),
      jsonrpc::json::parse_error);
}



TEST(ParseIncrementalParser, parse_same_twice)
{
  // This test string happens to be a problem in other code, so let's try to
  // reproduce it with this.
  auto text = R"({"id":42,"jsonrpc":"2.0","method":"echo","params":{"message":"Hello, world!"}})";

  using namespace jsonrpc::parse;
  using parser_type = incremental_parser<char>;

  std::size_t called = 0;
  parser_type parser{[&called](jsonrpc::json const &)
    {
      ++called;
    }
  };

  // First push of this should be as expected.
  parser.push(text, text + std::strlen(text));
  ASSERT_EQ(1, called);

  // Second push should only increment called once more.
  parser.push(text, text + std::strlen(text));
  ASSERT_EQ(2, called);
}
