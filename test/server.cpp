/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#include <gtest/gtest.h>

#include <build-config.h>

#include <sstream>

#include <json-rpc/server.h>
#include <json-rpc/request.h>
#include <json-rpc/response.h>

#include "../lib/parse/incremental_parser.h"

namespace {

struct response_parsing
{
  size_t          m_write_invoked = 0;
  size_t          m_succeeded = 0;
  std::optional<jsonrpc::response> m_response = {};

  std::string     m_expected_tag;

  jsonrpc::async_server_processor * m_proc = nullptr;
  jsonrpc::server m_server;

  inline response_parsing(std::string const & expected_tag)
    : m_expected_tag{expected_tag}
    , m_server{std::bind(&response_parsing::parse_func, this,
        std::placeholders::_1,
        std::placeholders::_2,
        std::placeholders::_3)}
  {
    m_server.on_log_output([](std::string const & msg)
        {
          std::cerr << "LOG: " << msg << std::endl;
        }
      );
  }


  inline response_parsing(std::string const & expected_tag,
      jsonrpc::async_server_processor * proc)
    : m_expected_tag{expected_tag}
    , m_proc{proc}
    , m_server{
        std::bind(&response_parsing::parse_func, this,
          std::placeholders::_1,
          std::placeholders::_2,
          std::placeholders::_3),
        *m_proc
      }
  {
    m_server.on_log_output([](std::string const & msg)
        {
          std::cerr << "LOG: " << msg << std::endl;
        }
      );
  }



  void parse_func(jsonrpc::io::peer_tag const & tag, char const * buf,
      size_t size)
  {
    ++m_write_invoked;
    ASSERT_EQ(m_expected_tag, tag);

    // Parse ID from request here, so we can create a response.
    jsonrpc::parse::incremental_parser<char> parser{
      [this](jsonrpc::json const & value)
      {
        jsonrpc::response resp{value};
        m_response = resp;
        ++m_succeeded;
      }
    };
    parser.push(buf, buf + size);
  }
};


struct echo_method
{
  std::string m_expected;
  size_t      m_invoked = 0;
  size_t      m_succeeded = 0;

  inline echo_method(std::string const & expected)
    : m_expected{expected}
  {
  }


  inline std::optional<jsonrpc::response>
  operator()(jsonrpc::request const & req)
  {
    ++m_invoked;
    EXPECT_EQ(m_expected, req.params["message"].get<std::string>());

    jsonrpc::json result = R"({})"_json;
    result["message"] = req.params["message"];
    jsonrpc::response resp{req.id, result};

    ++m_succeeded;
    return resp;
  }
};


inline jsonrpc::request
create_echo(std::string const & message)
{
  auto params = R"({})"_json;
  params["message"] = message;
  jsonrpc::request req{"echo", params};
  req.id = 42;
  return req;
}

} // anonymous namespace



TEST(Server, no_method)
{
  using namespace jsonrpc;

  response_parsing rp{"client"};

  // No method registered

  // Consume request
  std::stringstream sstream;
  sstream << create_echo("foo");
  auto buf = sstream.str();

  rp.m_server.consume("client", buf.c_str(), buf.size());

  ASSERT_EQ(rp.m_write_invoked, 1);
  ASSERT_EQ(rp.m_succeeded, 1);

  // Expect an error response
  ASSERT_TRUE(rp.m_response.has_value());
  auto resp = rp.m_response.value();
  ASSERT_TRUE(resp.is_error());

  ASSERT_EQ(resp.error.code, ERR_METHOD_NOT_FOUND);
}



TEST(Server, wrong_method)
{
  using namespace jsonrpc;

  response_parsing rp{"client"};

  // Register a method, but not the echo method.
  size_t something_invoked = 0;
  rp.m_server.register_method("something",
      [&something_invoked](request const &) -> std::optional<response>
      {
        ++something_invoked;
        return {};
      }
  );

  // Consume request
  std::stringstream sstream;
  sstream << create_echo("foo");
  auto buf = sstream.str();

  rp.m_server.consume("client", buf.c_str(), buf.size());

  ASSERT_EQ(rp.m_write_invoked, 1);
  ASSERT_EQ(rp.m_succeeded, 1);

  // Expect an error response
  ASSERT_TRUE(rp.m_response.has_value());
  auto resp = rp.m_response.value();
  ASSERT_TRUE(resp.is_error());

  ASSERT_EQ(resp.error.code, ERR_METHOD_NOT_FOUND);
}



TEST(Server, echo_method)
{
  using namespace jsonrpc;

  response_parsing rp{"client"};

  // Register an echo method
  echo_method echo{"Hello, world!"};
  rp.m_server.register_method("echo", std::ref(echo));

  // Consume request
  std::stringstream sstream;
  sstream << create_echo("Hello, world!");
  auto buf = sstream.str();

  rp.m_server.consume("client", buf.c_str(), buf.size());

  // Server method must have been invoked
  ASSERT_EQ(echo.m_invoked, 1);
  ASSERT_EQ(echo.m_succeeded, 1);

  ASSERT_EQ(rp.m_write_invoked, 1);
  ASSERT_EQ(rp.m_succeeded, 1);

  // Expect a non-error response
  ASSERT_TRUE(rp.m_response.has_value());
  auto resp = rp.m_response.value();
  ASSERT_FALSE(resp.is_error());

  // Check the response written to the I/O function!
  ASSERT_EQ(std::string{"Hello, world!"}, resp.result["message"].get<std::string>());
}



TEST(Server, echo_and_other_method)
{
  using namespace jsonrpc;

  response_parsing rp{"client"};

  // Register an echo method
  echo_method echo{"Hello, world!"};
  rp.m_server.register_method("echo", std::ref(echo));

  // Also register a dummy method
  size_t something_invoked = 0;
  rp.m_server.register_method("something",
      [&something_invoked](request const &) -> std::optional<response>
      {
        ++something_invoked;
        return {};
      }
  );

  // Consume request
  std::stringstream sstream;
  sstream << create_echo("Hello, world!");
  auto buf = sstream.str();

  rp.m_server.consume("client", buf.c_str(), buf.size());

  // Server method must have been invoked
  ASSERT_EQ(echo.m_invoked, 1);
  ASSERT_EQ(echo.m_succeeded, 1);
  ASSERT_EQ(something_invoked, 0);

  ASSERT_EQ(rp.m_write_invoked, 1);
  ASSERT_EQ(rp.m_succeeded, 1);

  // Expect a non-error response
  ASSERT_TRUE(rp.m_response.has_value());
  auto resp = rp.m_response.value();
  ASSERT_FALSE(resp.is_error());

  // Check the response written to the I/O function!
  ASSERT_EQ(std::string{"Hello, world!"}, resp.result["message"].get<std::string>());
}


TEST(Server, echo_and_other_with_erase)
{
  using namespace jsonrpc;

  response_parsing rp{"client"};

  // Register an echo method
  echo_method echo{"Hello, world!"};
  rp.m_server.register_method("echo", std::ref(echo));

  // Also register a dummy method
  size_t something_invoked = 0;
  rp.m_server.register_method("something",
      [&something_invoked](request const &) -> std::optional<response>
      {
        ++something_invoked;
        return {};
      }
  );

  // Consume request
  std::stringstream sstream;
  sstream << create_echo("Hello, world!");
  auto buf = sstream.str();

  rp.m_server.consume("client", buf.c_str(), buf.size());

  // Server method must have been invoked
  ASSERT_EQ(echo.m_invoked, 1);
  ASSERT_EQ(echo.m_succeeded, 1);
  ASSERT_EQ(something_invoked, 0);

  ASSERT_EQ(rp.m_write_invoked, 1);
  ASSERT_EQ(rp.m_succeeded, 1);

  // Expect a non-error response
  ASSERT_TRUE(rp.m_response.has_value());
  auto resp = rp.m_response.value();
  ASSERT_FALSE(resp.is_error());

  // Check the response written to the I/O function!
  ASSERT_EQ(std::string{"Hello, world!"}, resp.result["message"].get<std::string>());

  // Now erase the echo method.
  rp.m_server.erase_method("echo");

  // Consuming the same request again should not increase any callback
  // counters.
  rp.m_server.consume("client", buf.c_str(), buf.size());
  ASSERT_EQ(echo.m_invoked, 1);
  ASSERT_EQ(echo.m_succeeded, 1);
  ASSERT_EQ(something_invoked, 0);

  // ... except, of course, we'll have an error.
  ASSERT_EQ(rp.m_write_invoked, 2);
  ASSERT_EQ(rp.m_succeeded, 2);

  ASSERT_TRUE(rp.m_response.has_value());
  auto resp2 = rp.m_response.value();
  ASSERT_TRUE(resp2.is_error());

  ASSERT_EQ(resp2.error.code, ERR_METHOD_NOT_FOUND);
}


TEST(Server, flush_peer)
{
  using namespace jsonrpc;

  response_parsing rp{"client"};

  // Register an echo method
  echo_method echo{"Hello, world!"};
  rp.m_server.register_method("echo", std::ref(echo));

  // Consume incomplete request
  std::stringstream sstream_frag;
  sstream_frag << R"({"jsonrpc": "2.0",)";
  auto buf_frag = sstream_frag.str();
  rp.m_server.consume("client", buf_frag.c_str(), buf_frag.size());
  // Server method must not have been invoked
  ASSERT_EQ(echo.m_invoked, 0);
  ASSERT_EQ(echo.m_succeeded, 0);
  ASSERT_EQ(rp.m_write_invoked, 0);
  ASSERT_EQ(rp.m_succeeded, 0);

  // Flush/Remove peer
  rp.m_server.flush_peer("client");

  // Consume complete request with same tag (new parser should be created)
  std::stringstream sstream;
  sstream << create_echo("Hello, world!");
  auto buf = sstream.str();
  rp.m_server.consume("client", buf.c_str(), buf.size());

  // Server method must have been invoked
  ASSERT_EQ(echo.m_invoked, 1);
  ASSERT_EQ(echo.m_succeeded, 1);

  ASSERT_EQ(rp.m_write_invoked, 1);
  ASSERT_EQ(rp.m_succeeded, 1);

  // Expect a non-error response
  ASSERT_TRUE(rp.m_response.has_value());
  auto resp = rp.m_response.value();
  ASSERT_FALSE(resp.is_error());

  // Check the response written to the I/O function!
  ASSERT_EQ(std::string{"Hello, world!"}, resp.result["message"].get<std::string>());
}


TEST(Server, error_explicit)
{
  using namespace jsonrpc;

  response_parsing rp{"client"};

  // Register a method returning an explicit error
  rp.m_server.register_method("echo", [](request const & req) -> response
  {
    return {req.id, error{ERR_INVALID_PARAMS, "foo"}};
  });

  // Consume request
  std::stringstream sstream;
  sstream << create_echo("Hello, world!");
  auto buf = sstream.str();

  rp.m_server.consume("client", buf.c_str(), buf.size());

  // Server method must have been invoked
  ASSERT_EQ(rp.m_write_invoked, 1);
  ASSERT_EQ(rp.m_succeeded, 1);

  // Expect a non-error response
  ASSERT_TRUE(rp.m_response.has_value());
  auto resp = rp.m_response.value();
  ASSERT_TRUE(resp.is_error());
  ASSERT_EQ(resp.error.code, ERR_INVALID_PARAMS);
}


TEST(Server, error_throw)
{
  using namespace jsonrpc;

  response_parsing rp{"client"};

  // Register a method returning an explicit error
  rp.m_server.register_method("echo", [](request const &) -> response
  {
    throw error{ERR_INVALID_PARAMS, "foo"};
  });

  // Consume request
  std::stringstream sstream;
  sstream << create_echo("Hello, world!");
  auto buf = sstream.str();

  rp.m_server.consume("client", buf.c_str(), buf.size());

  // Server method must have been invoked
  ASSERT_EQ(rp.m_write_invoked, 1);
  ASSERT_EQ(rp.m_succeeded, 1);

  // Expect a non-error response
  ASSERT_TRUE(rp.m_response.has_value());
  auto resp = rp.m_response.value();
  ASSERT_TRUE(resp.is_error());
  ASSERT_EQ(resp.error.code, ERR_INVALID_PARAMS);
}


TEST(Server, error_stdexcept)
{
  using namespace jsonrpc;

  response_parsing rp{"client"};

  // Register a method returning an explicit error
  rp.m_server.register_method("echo", [](request const &) -> response
  {
    throw std::runtime_error{"error"};
  });

  // Consume request
  std::stringstream sstream;
  sstream << create_echo("Hello, world!");
  auto buf = sstream.str();

  rp.m_server.consume("client", buf.c_str(), buf.size());

  // Server method must have been invoked
  ASSERT_EQ(rp.m_write_invoked, 1);
  ASSERT_EQ(rp.m_succeeded, 1);

  // Expect a non-error response
  ASSERT_TRUE(rp.m_response.has_value());
  auto resp = rp.m_response.value();
  ASSERT_TRUE(resp.is_error());
  ASSERT_EQ(resp.error.code, ERR_INTERNAL);
}


TEST(Server, error_other)
{
  using namespace jsonrpc;

  response_parsing rp{"client"};

  // Register a method returning an explicit error
  rp.m_server.register_method("echo", [](request const &) -> response
  {
    throw std::string{"error"};
  });

  // Consume request
  std::stringstream sstream;
  sstream << create_echo("Hello, world!");
  auto buf = sstream.str();

  rp.m_server.consume("client", buf.c_str(), buf.size());

  // Server method must have been invoked
  ASSERT_EQ(rp.m_write_invoked, 1);
  ASSERT_EQ(rp.m_succeeded, 1);

  // Expect a non-error response
  ASSERT_TRUE(rp.m_response.has_value());
  auto resp = rp.m_response.value();
  ASSERT_TRUE(resp.is_error());
  ASSERT_EQ(resp.error.code, ERR_SERVER_UNKNOWN);
}


TEST(Server, error_from_parsing)
{
  using namespace jsonrpc;

  response_parsing rp{"client"};

  // Register a method returning an explicit error
  rp.m_server.register_method("echo", [](request const & req) -> response
  {
    // Try to treat a nonexistent parameter as a bool
    auto fail = req.params[0].get<bool>();
    auto res = R"({})"_json;
    res["fail"] = fail;
    return {req.id, fail};
  });

  // Consume request
  std::stringstream sstream;
  sstream << create_echo("Hello, world!");
  auto buf = sstream.str();

  rp.m_server.consume("client", buf.c_str(), buf.size());

  // Server method must have been invoked
  ASSERT_EQ(rp.m_write_invoked, 1);
  ASSERT_EQ(rp.m_succeeded, 1);

  // Expect a non-error response
  ASSERT_TRUE(rp.m_response.has_value());
  auto resp = rp.m_response.value();
  ASSERT_TRUE(resp.is_error());
  ASSERT_EQ(resp.error.code, ERR_INTERNAL);
}



TEST(Server, echo_method_async)
{
#if defined(JSON_RPC_ENABLE_ASYNC_SERVER)
  using namespace jsonrpc;

  async_server_processor asyncproc;

  response_parsing rp{"client", &asyncproc};

  // Register an echo method
  echo_method echo{"Hello, world!"};
  rp.m_server.register_method("echo", std::ref(echo));

  // Consume request
  std::stringstream sstream;
  sstream << create_echo("Hello, world!");
  auto buf = sstream.str();

  rp.m_server.consume("client", buf.c_str(), buf.size());

  ASSERT_EQ(echo.m_invoked, 0);
  ASSERT_EQ(echo.m_succeeded, 0);

  ASSERT_EQ(rp.m_write_invoked, 0);
  ASSERT_EQ(rp.m_succeeded, 0);

  using namespace std::chrono_literals;
  auto amount = asyncproc.poll_results(100ms);
  ASSERT_EQ(amount, 1);

  // Server method must have been invoked
  ASSERT_EQ(echo.m_invoked, 1);
  ASSERT_EQ(echo.m_succeeded, 1);

  ASSERT_EQ(rp.m_write_invoked, 1);
  ASSERT_EQ(rp.m_succeeded, 1);

  // Expect a non-error response
  ASSERT_TRUE(rp.m_response.has_value());
  auto resp = rp.m_response.value();
  ASSERT_FALSE(resp.is_error());

  // Check the response written to the I/O function!
  ASSERT_EQ(std::string{"Hello, world!"}, resp.result["message"].get<std::string>());
#else
  GTEST_SKIP() << "Set enable_async_server in meson_options.txt to enable.";
#endif
}

