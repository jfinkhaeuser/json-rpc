/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#include <gtest/gtest.h>

#include <json-rpc/error.h>


TEST(Error, bad_input)
{
  jsonrpc::error err{R"({
    "jsonrpc": "2.0",
    "method": "foo",
    "params": [42, 128],
    "id": 123
  })"_json};

  ASSERT_FALSE(err.is_valid());
}


TEST(Error, with_reserved_code)
{
  jsonrpc::error err{R"({
    "code": -32700,
    "message": "Something bad happened!"
  })"_json};

  ASSERT_TRUE(err.is_valid());

  std::string what = err.what();

  ASSERT_NE(std::string::npos, what.find("-32700"));
  ASSERT_NE(std::string::npos, what.find("Parse"));
  ASSERT_NE(std::string::npos, what.find("Something bad happened!"));
}


TEST(Error, with_custom_code)
{
  jsonrpc::error err{R"({
    "code": 42,
    "message": "Something bad happened!"
  })"_json};

  ASSERT_TRUE(err.is_valid());

  std::string what = err.what();

  ASSERT_NE(std::string::npos, what.find("[42]"));
  ASSERT_NE(std::string::npos, what.find("Something bad happened!"));
}


TEST(Error, as_exception)
{
  try {
    throw jsonrpc::error{jsonrpc::ERR_METHOD_NOT_FOUND, "yep"};
  } catch (std::exception const & ex) {
    std::string what = ex.what();

    ASSERT_NE(std::string::npos, what.find("-32601"));
    ASSERT_NE(std::string::npos, what.find("yep"));
  }
}


TEST(Error, as_exception_with_data)
{
  try {
    throw jsonrpc::error{jsonrpc::ERR_METHOD_NOT_FOUND, "yep",
      R"({"some": "data"})"_json};
  } catch (jsonrpc::error const & ex) {
    std::string what = ex.what();

    ASSERT_NE(std::string::npos, what.find("-32601"));
    ASSERT_NE(std::string::npos, what.find("yep"));

    ASSERT_FALSE(ex.data.empty());
    ASSERT_TRUE(ex.data.contains("some"));
    ASSERT_TRUE(ex.data["some"].is_string());
    ASSERT_EQ("data", ex.data["some"].get<std::string>());
  }
}
