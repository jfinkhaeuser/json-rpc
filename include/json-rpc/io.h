/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef JSON_RPC_IO_H
#define JSON_RPC_IO_H

#include <json-rpc.h>

#include <functional>
#include <string>

namespace jsonrpc::io {


/**
 * The types defined here are the interface between this library and an I/O
 * subsystem of your choice.
 *
 * Communications with a peer by any means needs to identify the peer for
 * client or server to be able to handle multiple peers. To simplify matters,
 * this peer_tag is a character sequence. The contents are opaque to the
 * library, but using a character sequence means it can be logged.
 */
using peer_tag = std::string;

/**
 * When client or server generate data to send to a peer, they will pass the
 * peer tag and the data to a callback function. You have a choice of a
 * functions here. However, they all essentially accept a byte sequence. To
 * make matters a little simpler, this can be either a void pointer and size
 * in Bytes, a char pointer and size, or a char range. In practice, the library
 * uses only the void pointer function internally, and generates lambdas for
 * your convenience when you choose to provide a different function.
 */
using write_void_buf = std::function<
  void (peer_tag const &, void const *, size_t)
>;

using write_char_buf = std::function<
  void (peer_tag const &, char const *, size_t)
>;

using write_char_range = std::function<
  void (peer_tag const &, char const *, char const *)
>;

} // namespace jsonrpc::io

#endif // guard
