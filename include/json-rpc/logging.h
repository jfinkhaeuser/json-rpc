/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef JSON_RPC_LOGGING_H
#define JSON_RPC_LOGGING_H

#include <json-rpc.h>

#include <functional>
#include <string>
#include <sstream>

namespace jsonrpc::log {

/**
 * Client and server both may want to log some information at some times.
 * The logging needs are *very* few, which also means that there is no real
 * log severity difference - they're practically all debug log messages.
 *
 * So we'll define a log function prototype here that can be passed to client
 * and server. It's probably safe to consider it for debug logging purposes
 * only.
 */
using function = std::function<void (std::string const &)>;


/**
 * The logger class provides a minimal interface for ensuring that stream
 * operations are not executed unsless a log function is known. Use temporary
 * objects of this type for logging, i.e.:
 *
 *  logger(func) << "foo" << "bar";
 */
struct logger
{
  function          m_logfunc;
  std::stringstream m_stream;

  inline explicit logger(function logfunc)
    : m_logfunc{logfunc}
  {
  }

  inline ~logger()
  {
    auto contents = m_stream.str();
    if (contents.empty()) {
      return;
    }
    m_logfunc(contents);
  }

  template <typename T>
  inline logger &
  operator<<(T const & v)
  {
    if (!m_logfunc) {
      return *this;
    }

    m_stream << v;
    return *this;
  }
};

} // namespace jsonrpc::log

#endif // guard
