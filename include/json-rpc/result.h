/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef JSON_RPC_RESULT_H
#define JSON_RPC_RESULT_H

#include <json-rpc.h>

#include <tuple>
#include <optional>

#include <json-rpc/response.h>

namespace jsonrpc {

/**
 * Processing JSON as JSON-RPC responses can yield a number of different
 * result states, listed below. These states are used internally, and are
 * reported to e.g. client callbacks.
 */
enum result_state : int
{
  // A valid response.
  RE_OK_RESPONSE        = 0,
  // A response that contains en error object.
  RE_ERROR_RESPONSE     = 1,
  // A response that does not match any known request IDs.
  RE_UNKNOWN_REQUEST_ID = 2,
  // Some kind of error in processing the JSON.
  RE_PROCESSING_ERROR   = 3,
  // A timeout occurred.
  RE_TIMEOUT            = 4,
};

/**
 * A result is a tuple of a result_state and an optional response.
 */
template <
  typename batonT = void *
>
using result = std::tuple<
  result_state,
  std::optional<response>,
  batonT
>;

} // namespace jsonrpc

#endif // guard
