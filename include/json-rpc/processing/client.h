/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef JSON_RPC_PROCESSING_CLIENT_H
#define JSON_RPC_PROCESSING_CLIENT_H

#include <json-rpc.h>

#include <functional>
#include <map>
#include <set>
#include <vector>
#include <optional>

#include <json-rpc/result.h>
#include <json-rpc/request.h>
#include <json-rpc/response.h>
#include <json-rpc/clock.h>

namespace jsonrpc::processing {


/**
 * The processing client generates valid request objects for us from a method
 * name and parameters; the main point here is generate non-conflicting request
 * identifiers.
 *
 * Request identifiers that are in-flight are recorded. Feeding a corresponding
 * response object to the client will remove the in-flight request identifier,
 * and return a success or status result.
 */
template <
  typename batonT = void *
>
struct client
{
  using id_type = size_t;
  using result_vec = std::vector<result<batonT>>;


  inline explicit client(duration const & timeout = duration::max())
    : m_timeout{timeout}
  {
  }


  /**
   * Create a request or notification.
   */
  inline request create_request(std::string const & method,
      json const & params = {},
      relative_time_point const & now = {},
      batonT const & baton = batonT{})
  {
    auto res = create_notification(method, params);
    res.id = generate_and_remember_id(now, baton);
    return res;
  }

  inline request create_notification(std::string const & method,
      json const & params = {})
  {
    return request{method, params};
  }

  /**
   * Receive a response.
   */
  inline result_vec
  receive_response(response const & resp,
      relative_time_point const & now = {},
      batonT const & baton = batonT{})
  {
    result_vec results;
    results.push_back(process_response(resp, now, baton));

    handle_timeouts(results, now);
    return results;
  }


  /**
   * Receive a list of responses. Much as in the server example, this is
   * more useful for testing than anything else, but it's still useful.
   */
  inline result_vec
  receive_responses(std::vector<response> const & responses,
      relative_time_point const & now = {},
      batonT const & baton = batonT{})
  {
    result_vec results;
    for (auto resp : responses) {
      // cppcheck-suppress useStlAlgorithm
      results.push_back(process_response(resp, now, baton));
    }

    handle_timeouts(results, now);
    return results;
  }


  /**
   * Just process timeouts without receiving a response.
   */
  inline result_vec
  process_timeouts(relative_time_point const & now)
  {
    result_vec results;
    handle_timeouts(results, now);
    return results;
  }


  /**
   * Query in-flight requests.
   */
  inline std::size_t num_in_flight() const
  {
    return m_in_flight.size();
  }

  inline bool is_in_flight(id_type id) const
  {
    return (m_in_flight.find(id) != m_in_flight.end());
  }

  inline bool is_in_flight(json const & id) const
  {
    if (!id.is_number()) {
      return false;
    }
    return is_in_flight(id.get<id_type>());
  }

  /**
   * As a convenience, we'll add a batch builder.
   *
   * Note that this is *not* transactional; that is, each request added here
   * is immediately registered with the client.
   */
  struct batch_builder
  {
    inline batch_builder &
    create_request(std::string const & method, json const & params = {},
        relative_time_point const & now = {},
        batonT const & baton = batonT{}
      )
    {
      auto req = m_client.create_request(method, params, now, baton);
      m_result.push_back(req);
      return *this;
    }

    inline batch_builder &
    create_notification(std::string const & method, json const & params = {})
    {
      auto req = m_client.create_notification(method, params);
      m_result.push_back(req);
      return *this;
    }

    inline std::vector<request>
    get() const
    {
      return m_result;
    }

  private:
    friend struct client;

    inline explicit batch_builder(client & _client)
      : m_client{_client}
      , m_result{}
    {
    }

    client &              m_client;
    std::vector<request>  m_result;
  };

  inline batch_builder batch()
  {
    return batch_builder{*this};
  }

private:

  inline id_type generate_id()
  {
    std::hash<std::string> hasher;
    return hasher(std::to_string(++m_seed));
  }

  inline id_type generate_and_remember_id(relative_time_point const & now,
      batonT const & baton)
  {
    // Loop ID generation until the result is non-conflicting; this should
    // generally be done in a single iteration, but std::hash doesn't really
    // guarantee it.
    id_type id;
    auto iter = m_in_flight.begin();
    do {
      id = generate_id();
      iter = m_in_flight.find(id);
    } while (iter != m_in_flight.end());

    // Store ID and return it. We need to store the relative_time_point at
    // which the request would expire, too.
    m_in_flight.insert(
        typename in_flight_map::value_type{
          id,
          std::make_tuple(calculate_timeout(now), baton)
        }
    );
    return id;
  }


  inline relative_time_point calculate_timeout(relative_time_point const & now)
  {
    // If now is unset, we're going to assume that timeouts are disabled
    // here - which means we'll have to return the maximum timeout. Note that
    // this does *not* handle clock wraparound at all, so let's hope your clock
    // is for a long enough period.
    if (now == relative_time_point{}) {
      return relative_time_point::max();
    }

    // If the configured timeout is set to maximum, we also consider timeouts
    // to be disabled.
    if (m_timeout == duration::max()) {
      return relative_time_point::max();
    }

    // At this point, the effective timeout is easy to calculate - again,
    // though, we do nothing to prevent clock wraparound.
    return now + m_timeout;
  }


  inline bool is_expired(relative_time_point const & timeout,
      relative_time_point const & now)
  {
    // If now is unset, we assume timeouts are not important.
    if (now == relative_time_point{}) {
      return false;
    }

    // If the timeout is on or before now, the request is expired.
    return (timeout <= now);
  }


  inline void handle_timeouts(result_vec & results,
      relative_time_point const & now)
  {
    // Produce a response for every expired in-flight request. We cannot
    // provide much in the response, but we do want to have the request
    // ID in there.
    std::set<id_type> to_erase;
    for (auto & [id, in_flight] : m_in_flight) {
      auto & [timeout, baton] = in_flight;
      if (!is_expired(timeout, now)) {
        continue;
      }

      // Ok, so first thing is we want to erase this request from the in-flight
      // ones.
      to_erase.insert(id);

      // Next, create an error - this will let us provide *some* response.
      // There is no timeout error code, so we'll just use ERR_INTERNAL -
      // the timeout is already part of the status.
      error err{ERR_INTERNAL, "Request timed out, client is giving up."};

      // Add the status and response to the results.
      response resp{id, err};
      results.push_back({RE_TIMEOUT, resp, baton});
    }

    // When the map is done processing, erase all expired IDs.
    for (auto & id : to_erase) {
      m_in_flight.erase(id);
    }
  }


  inline result<batonT>
  process_response(response const & resp, relative_time_point const & now = {},
      batonT const & receive_baton = batonT{})
  {
    // Check if the response is valid.
    if (!resp.is_valid()) {
      return {RE_PROCESSING_ERROR, resp, receive_baton};
    }

    // First things first: can we find the ID in in-flight calls? For that,
    // we need to have a numerical ID.
    if (!resp.id.is_number()) {
      return {RE_UNKNOWN_REQUEST_ID, resp, receive_baton};
    }
    auto id = resp.id.get<id_type>();
    auto iter = m_in_flight.find(id);
    if (iter == m_in_flight.end()) {
      // Not expired, just unknown
      return {RE_UNKNOWN_REQUEST_ID, resp, receive_baton};
    }

    auto [timeout, baton] = iter->second;

    // The request is in-flight, but it may be expired. We'll have to check the
    // timeout here. If it's not expired, we need to return the status based on
    // whether it's an OK or error response.
    auto state = resp.is_error() ? RE_ERROR_RESPONSE : RE_OK_RESPONSE;
    if (is_expired(timeout, now)) {
      state = RE_TIMEOUT;
    }

    // So the request is in-flight, which means all is well. Remove it.
    m_in_flight.erase(iter);

    // Now just return the status and response. Which baton should we use?
    // Typically, if the receive_baton is set, it should be identical to the
    // stored one. If not, that is an internal error.
    if (receive_baton != batonT{} && receive_baton != baton) {
      // We'll return the receive baton here - this is because that is the
      // value that caused an issue.
      return {RE_PROCESSING_ERROR, resp, receive_baton};
    }

    return {state, resp, baton};
  }



  using in_flight_value = std::tuple<relative_time_point, batonT>;
  using in_flight_map = std::map<id_type, in_flight_value>;
  in_flight_map     m_in_flight = {};
  id_type           m_seed = 0;
  duration          m_timeout;
};

} // namespace jsonrpc::processing

#endif // guard
