/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef JSON_RPC_PROCESSING_SERVER_H
#define JSON_RPC_PROCESSING_SERVER_H

#include <json-rpc.h>

#include <functional>
#include <map>
#include <vector>
#include <optional>

#include <json-rpc/request.h>
#include <json-rpc/response.h>
#include <json-rpc/method.h>

namespace jsonrpc::processing {


/**
 * The main algorithmic component in this implementation is the server
 * class here. It registeres callbacks for method names, and provides the
 * code handling errors in processing.
 *
 * Note that the server *in no way* tries to ensure non-conflicting request
 * IDs. It's a purely synchronous processor. If a client issues multiple
 * requests with conflicting request identifiers, that's the client's
 * problem.
 */
struct server
{
  /**
   * Technically, any list-like type could be used here, but let's keep things
   * simple.
   */
  using request_list = std::vector<request>;
  using response_list = std::vector<response>;

  /**
   * Methods can be overwritten or cleared from the method map. Typically, you
   * would only do this once at startup and not modify the map during run-time.
   */
  inline bool register_method(std::string const & name, method code, bool overwrite = false)
  {
    if (!overwrite) {
      auto iter = m_methods.find(name);
      if (iter != m_methods.end()) {
        return false;
      }
    }

    m_methods[name] = code;
    return true;
  }

  inline bool erase(std::string const & name)
  {
    auto iter = m_methods.find(name);
    if (iter == m_methods.end()) {
      return false;
    }
    m_methods.erase(iter);
    return true;
  }


  /**
   * This function is public mostly for testing purposes; it invokes a single
   * request method (if a method is registered). It also catches any errors
   * during execution, and translates them into valid response objects.
   *
   * We use an optional return type because notifications do not produce
   * results.
   */
  inline std::optional<response>
  invoke(request const & request) const
  try {
    // All parsing has been done. First find the method.
    auto iter = m_methods.find(request.method);
    if (iter == m_methods.end()) {
      error err{ERR_METHOD_NOT_FOUND, "Not found: " + request.method};
      return response{request.id, err};
    }

    // Invoke the method. We'll keep the result for a moment, because we can
    // make sure it's got the right ID set.
    auto res = iter->second(request);
    if (res.has_value()) {
      res.value().id = request.id;
    }

    return res;
  } catch (error const & ex) {
    // JSON-RPC errors can be convereted immediately to a response
    return response{request.id, ex};
  } catch (std::exception const & ex) {
    // For std::exception, let's at least get the message. This may
    // include JSON parsing exceptions, which is okay because most
    // parsing should be done *outside* of this function, so this is
    // more about accessing the passed JSON parameters or some such.
    error err{ERR_INTERNAL, ex.what()};
    return response{request.id, err};
  } catch (...) {
    // For all other exceptions, we don't really know what to
    // return.
    error err{ERR_SERVER_UNKNOWN, "An unexpected error type was caught."};
    return response{request.id, err};
  }


  /**
   * Since parsing of the request list itself is already done, this batch
   * invocation method produces exactly the same amount of responses as
   * requests in a list.
   *
   * Note that this function is largely useful for testing purposes. When
   * a server encounters a batch request with a valid JSON but invalid
   * Request object, it needs to return an error response for that array
   * entry. That is not possible here, because only valid request objects
   * are accepted; parsing is done prior to this function.
   */
  inline response_list invoke(request_list const & requests) const
  {
    response_list results;
    for (auto const & req : requests) {
      auto res = invoke(req);
      if (res.has_value()) {
        results.push_back(res.value());
      }
    }
    return results;
  }


private:
  using method_map = std::map<std::string, method>;
  method_map  m_methods = {};
};

} // namespace jsonrpc::processing

#endif // guard
