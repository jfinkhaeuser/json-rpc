/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef JSON_RPC_SERVER_H
#define JSON_RPC_SERVER_H

#include <json-rpc.h>

#include <functional>
#include <memory>
#include <optional>
#include <chrono>

#include <json-rpc/request.h>
#include <json-rpc/response.h>
#include <json-rpc/method.h>
#include <json-rpc/io.h>
#include <json-rpc/logging.h>

namespace jsonrpc {

/**
 * The server class handles JSON-RPC server functionality without dealing
 * with I/O.
 *
 * The interface to an I/O subsystem is the same as for the client class;
 * see this and/or the io.h header for details.
 *
 * Servers differ from clients largely in how callbacks are handled.
 * Instead of registering response callbacks, methods with the above prototype
 * are provided.
 *
 * Each method is registered under a given name. In a given JSON-RPC request,
 * the method name is matched against these registered names, and the request
 * dispatched to the function associated with the name.
 *
 * You can create an error response in these methods, or not return any
 * response i.e. when handling notifications.
 */
class JSON_RPC_API server
{
public:
  /**
   * The server can optionally accept a processor implementation. If none is
   * given, a simple, synchronous processor is instanciated.
   */
  struct processor
  {
    using result_func = std::function<void (io::peer_tag const &, response const &)>;

    virtual ~processor() = default;

    virtual bool register_method(std::string const & name, method code, bool overwrite = false) = 0;
    virtual bool erase_method(std::string const & name) = 0;
    virtual void invoke(io::peer_tag const & tag, request const & req, result_func result) = 0;

    virtual void on_log_output(log::function logfunc [[maybe_unused]])
    {
    };
  };

  /**
   * Register a write function in the constructor.
   *
   * Note: the buffer passed to the write function does *not* take ownership
   * of the passed buffer. This means write functions must either copy the
   * buffer contents, or process them immediately.
   */
  explicit server(io::write_void_buf func);
  server(io::write_void_buf func, processor & proc);

  inline explicit server(io::write_char_buf func)
    : server{
        io::write_void_buf{
          [func](io::peer_tag const & tag, void const * buf, size_t size)
          {
            func(tag, static_cast<char const *>(buf), size);
          }
        }
      }
  {
  }

  inline server(io::write_char_buf func, processor & proc)
    : server{
        io::write_void_buf{
          [func](io::peer_tag const & tag, void const * buf, size_t size)
          {
            func(tag, static_cast<char const *>(buf), size);
          }
        },
        proc
      }
  {
  }


  inline explicit server(io::write_char_range func, processor & proc)
    : server{
        io::write_void_buf{
          [func](io::peer_tag const & tag, void const * buf, size_t size)
          {
            func(tag, static_cast<char const *>(buf),
                static_cast<char const *>(buf) + size);
          }
        },
        proc
      }
  {
  }


  ~server() = default;

  server(server const &) = default;
  server(server &&) = default;
  server & operator=(server const &) = default;


  /**
   * Register and erase methods, associated with an RPC method name. When
   * registering a method, you can ask to overwrite already registered methods
   * under this name.
   *
   * The functions return whether the given method has been registered or
   * erased.
   */
  bool register_method(std::string const & name, method code, bool overwrite = false);
  bool erase_method(std::string const & name);


  /**
   * Register an (optional) log function.
   */
  server & on_log_output(log::function logfunc);


  /**
   * Remove the parser for a given tag from the parser map
   */
  void flush_peer(io::peer_tag const & tag);


  /**
   * The counterpart to the write functions; call this with any data read
   * from a peer. This will process any responses serialized within the
   * buffer, and invoke response callbacks as appropriate.
   */
  void consume(io::peer_tag const & tag, void const * buf, size_t size);

  inline void
  consume(io::peer_tag const & tag, char const * buf, size_t size)
  {
    consume(tag, static_cast<void const *>(buf), size);
  }

  inline void
  consume(io::peer_tag const & tag, char const * begin, char const * end)
  {
    consume(tag, begin, (end - begin));
  }


  /**
   * Simple wrapper around the write function; this helps external processors
   * use the I/O capabilities of this class to write responses.
   */
  void produce(io::peer_tag const & tag, response const & res);



private:
  struct server_impl;
  std::shared_ptr<server_impl> m_impl;
};



/**
 * We provide an asynchronous request processor.
 *
 * The constructor will raise if the `enable_command_queue` is not set. If this
 * library is built with the command queue feature, a processor that operates in
 * a single background thread is created, and commands are passed to this thread
 * via a command queue.
 *
 * Being asynchronous, the invoke() function will not immediately result in the
 * request being processed, and the passed result function being called. This
 * may be delayed. Call poll_results() periodically to perform that last step of
 * post processing.
 */
class JSON_RPC_API async_server_processor : public server::processor
{
public:
  async_server_processor();
  virtual ~async_server_processor() = default;

  virtual bool register_method(std::string const & name, method code, bool overwrite = false) override final;
  virtual bool erase_method(std::string const & name) override final;
  virtual void invoke(io::peer_tag const & tag, request const & req, result_func result) override final;
  virtual void on_log_output(log::function logfunc) override final;

  /**
   * Poll for results, waiting *at most* the given number of milliseconds for
   * fresh results to be posted. Return the number of results processed.
   */
  std::size_t poll_results(std::chrono::milliseconds const & wait_for = {});

private:
  struct impl;
  std::shared_ptr<impl>  m_impl;
};

} // namespace jsonrpc

#endif // guard
