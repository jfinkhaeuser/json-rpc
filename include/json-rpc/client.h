/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef JSON_RPC_CLIENT_H
#define JSON_RPC_CLIENT_H

#include <json-rpc.h>

#include <functional>
#include <map>
#include <vector>
#include <optional>

#include <json-rpc/result.h>
#include <json-rpc/request.h>
#include <json-rpc/response.h>
#include <json-rpc/io.h>
#include <json-rpc/logging.h>
#include <json-rpc/clock.h>

namespace jsonrpc {

/**
 * The client class handles JSON-RPC client functionality without dealing
 * with I/O.
 *
 * On the one hand, it allows you to create requests. Creating requests results
 * in serializing the request data, and invoking an I/O callback with the
 * resulting byte sequence. It is up to the I/O callback to transmit this to
 * another instance.
 *
 * On the other hand, you can feed it raw byte sequences. It will try to parse
 * responses, and associate them with pending requests. If a response has been
 * received, it will invoke callbacks with the parsed JSON data as registered
 * here.
 *
 * The client may have a timeout value configured. If a request is in-flight
 * for longer, it will expire, and result in a RE_TIMEOUT response - even if
 * the server sends a response later. Note that for this reason, client
 * functions optionally accept a relative_time_point - if none is provided,
 * then timeouts are ignored.
 *
 * Note also that this mechanism triggers when consume() is called. If you
 * have no data to consume, you can periodically call process_timeouts()
 * instead.
 */
class JSON_RPC_API client
{
public:
  // Callback for received responses.
  using response_callback = std::function<
    void (io::peer_tag const &, result_state, std::optional<response>)
  >;


  /**
   * Register a write function in the constructor.
   *
   * Note: the buffer passed to the write function does *not* take ownership
   * of the passed buffer. This means write functions must either copy the
   * buffer contents, or process them immediately.
   */
  explicit client(io::write_void_buf func,
      duration const & timeout = duration::max());

  inline explicit client(io::write_char_buf func,
      duration const & timeout = duration::max())
    : client{
        io::write_void_buf{
          [func](io::peer_tag const & tag, void const * buf, size_t size)
          {
            func(tag, static_cast<char const *>(buf), size);
          }
        },
        timeout
      }
  {
  }

  inline explicit client(io::write_char_range func,
      duration const & timeout = duration::max())
    : client{
        io::write_void_buf{
          [func](io::peer_tag const & tag, void const * buf, size_t size)
          {
            func(tag, static_cast<char const *>(buf),
                static_cast<char const *>(buf) + size);
          }
        },
        timeout
      }
  {
  }


  ~client() = default;

  client(client const &) = default;
  client(client &&) = default;
  client & operator=(client const &) = default;



  /**
   * Set rsponse callbacks. The version of the function with a peer_tag
   * parameter is only invoked when a response for this particular tag
   * arrives.
   *
   * The order of processing responses is as follows:
   * - If a request callback (see send_request() below) is set and matches
   *   the response, it is invoked and subsequently removed from the known
   *   request callbacks.
   * - If not, and a tag callback matches the response, it is invoked instead.
   * - If not, and a generic callback is set, it is invoked instead.
   */
  client & on_response(response_callback cb);
  client & on_response(io::peer_tag const & tag, response_callback cb);


  /**
   * Register an (optional) log function.
   */
  client & on_log_output(log::function logfunc);


  /**
   * Send a request or notification. This will invoke the write callback with
   * the given tag and serialized request. If a request is sent and a response
   * callback is provided, this will be invoked for a matching response.
   */
  void send_request(io::peer_tag const & tag, std::string const & method_name,
      json const & params = {}, response_callback cb = {},
      relative_time_point const & now = {});

  void send_notification(io::peer_tag const & tag,
      std::string const & method_name,
      json const & params = {});


  /**
   * The counterpart to the write functions; call this with any data read
   * from a peer. This will process any responses serialized within the
   * buffer, and invoke response callbacks as appropriate.
   *
   * This function also processes timeouts (see below).
   */
  void consume(io::peer_tag const & tag, void const * buf, size_t size,
      relative_time_point const & now = {});

  inline void
  consume(io::peer_tag const & tag, char const * buf, size_t size,
      relative_time_point const & now = {})
  {
    consume(tag, static_cast<void const *>(buf), size, now);
  }

  inline void
  consume(io::peer_tag const & tag, char const * begin, char const * end,
      relative_time_point const & now = {})
  {
    consume(tag, begin, (end - begin), now);
  }


  /**
   * Process timeouts. Invoke this function periodically if you have no data
   * to consume. Note that this function cannot invoke tagged callbacks with
   * a timeout event.
   */
  void process_timeouts(relative_time_point const & now);


private:
  struct client_impl;
  std::shared_ptr<client_impl>  m_impl;
};

} // namespace jsonrpc

#endif // guard
