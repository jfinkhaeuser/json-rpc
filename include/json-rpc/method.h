/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef JSON_RPC_METHOD_H
#define JSON_RPC_METHOD_H

#include <json-rpc.h>

#include <functional>
#include <optional>

#include <json-rpc/request.h>
#include <json-rpc/response.h>

namespace jsonrpc {

/**
 * The method type accepts a request object and returns a response object.
 * It may raise an error object, or any other kind of exception.
 *
 * We use an optional return type because notifications do not produce
 * results.
 *
 * One of the nice side-effects is that you can register methods just
 * returning a response, without the optional wrapper.
 */
using method = std::function<std::optional<response> (request const &)>;

} // namespace jsonrpc

#endif // guard
