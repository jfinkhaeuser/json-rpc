/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef JSON_RPC_ERROR_H
#define JSON_RPC_ERROR_H

#include <json-rpc.h>

#include <stdexcept>

namespace jsonrpc {

/**
 * Error codes have reserved values only up until negative 16 bits, so we'll
 * make it a signed 16 bit type.
 */
using error_code = int16_t;

/**
 * Some error code/ranges are predefined by the specs
 */
enum errors : error_code
{
  ERR_PARSE_ERROR       = -32700, // Invalid JSON was received by the server.
  ERR_INVALID_REQUEST   = -32600, // The JSON sent is not a valid Request object.
  ERR_METHOD_NOT_FOUND  = -32601, // The method does not exist / is not available.
  ERR_INVALID_PARAMS    = -32602, // Invalid method parameter(s).
  ERR_INTERNAL          = -32603, // Internal JSON-RPC error.

  // Reserved range
  ERR_RESERVED_START    = -32768,
  ERR_RESERVED_END      = -32000,

  // Reserved for implementation-defined server-errors.
  ERR_SERVER_START      = -32099,
  ERR_SERVER_END        = -32000,

  // We define an undefined error (error object is not valid), and an unknown
  // error.
  ERR_SERVER_UNDEFINED  = -32099,
  ERR_SERVER_UNKNOWN    = -32098,

  // The effective start for actual use must be after our own server-defined
  // error codes.
  ERR_SERVER_EFFECTIVE_START = -32097,
};


/**
 * Much like the request structure the error is very lightweight and
 * largely provides validation and some accessors.
 *
 * Unlike the request structure, this class is derived from
 * std::exception and therefore works as a regular C++ exception.
 */
class error : public std::exception
{
public:
  // Mandatory integer error code
  error_code    code = ERR_SERVER_UNDEFINED;

  // Mandatory string message
  std::string   message = {};

  // Optional data
  json          data;


  /**
   * Construction and copying
   */
  inline explicit error(json const & input)
    : m_error{""}
  {
    if (validate_error(input)) {
      code = input["code"].get<std::int32_t>();
      message = input["message"].get<std::string>();
      if (input.contains("data")) {
        data = input["data"];
      }

      create_what();
    }
  }

  // The what_arg must be anything that's convertible to std::string and
  // accepted by std::runtime_error's constructor. This includes string
  // types, and little else.
  template <typename T>
  inline error(error_code _code, T const & what_arg,
      json _data = {})
    : code{_code}
    , message{what_arg}
    , m_error{""}
  {
    // In ctor, this creates an array with the _data entry
    data = _data; // cppcheck-suppress useInitializationList
    create_what();
  }

  inline error()
    : m_error{""}
  {}

  inline error(error const &) = default;
  inline error(error &&) = default;
  inline error & operator=(error const &) = default;

  /**
   * Behave like std::runtime_error
   */
  char const * what() const noexcept override
  {
    return m_error.what();
  }

  /**
   * Serialize to JSON again.
   */
  inline json to_json() const
  {
    auto ret = R"({})"_json;
    ret["code"] = code;
    ret["message"] = message;
    if (!data.is_null()) {
      ret["data"] = data;
    }
    return ret;
  }

  /**
   * Validate a JSON object as a error
   */
  static inline bool validate_error(json const & input)
  {
    // Must be an object.
    if (!input.is_object()) {
      return false;
    }

    // Must have 2-3 keys.
    auto size = input.size();
    if (2 > size || size > 3) {
      return false;
    }

    // Must contain a "code" that is an integer
    if (!input.contains("code")) {
      return false;
    }
    if (!input["code"].is_number()) {
      return false;
    }

    // Must contain a non-empty string message
    if (!input.contains("message")) {
      return false;
    }
    if (!input["message"].is_string()) {
      return false;
    }
    if (input["message"].empty()) {
      return false;
    }

    // May contain a "data" member.
    if (size > 2) {
      if (!input.contains("data")) {
        return false;
      }
    }

    // Passed all checks
    return true;
  }


  /**
   * Convenience accessors
   */
  inline bool is_valid() const
  {
    return code != ERR_SERVER_UNDEFINED;
  }

private:

  inline void create_what()
  {
    // Create the what() part for the error message.
    std::string msg;

    switch (code) {
      case ERR_PARSE_ERROR:
        msg += "[Parse Error (" + std::to_string(code) + ")] ";
        break;

      case ERR_INVALID_REQUEST:
        msg += "[Invalid Request (" + std::to_string(code) + ")] ";
        break;

      case ERR_METHOD_NOT_FOUND:
        msg += "[Method Not Found (" + std::to_string(code) + ")] ";
        break;

      case ERR_INVALID_PARAMS:
        msg += "[Invalid Params (" + std::to_string(code) + ")] ";
        break;

      case ERR_INTERNAL:
        msg += "[Internal Error (" + std::to_string(code) + ")] ";
        break;

      case ERR_SERVER_UNDEFINED:
        msg += "[Undefined Server Error (" + std::to_string(code) + ")] ";
        break;

      case ERR_SERVER_UNKNOWN:
        msg += "[Unknown Server Error (" + std::to_string(code) + ")] ";
        break;

      default:
        msg += "[" + std::to_string(code) + "] ";
    }

    msg += message;

    m_error = std::runtime_error{msg};
  }

  // Runtime error as storage for arbitrary strings
  std::runtime_error m_error;
};


} // namespace jsonrpc

#endif // guard
