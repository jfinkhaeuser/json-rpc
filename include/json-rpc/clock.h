/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef JSON_RPC_CLOCK_H
#define JSON_RPC_CLOCK_H

#include <json-rpc.h>

#include <chrono>

namespace jsonrpc {

/**
 * In order to provide some kind of timeout handling, we need to know the time.
 * But chrono defines different clocks for different uses, and it's not always
 * clear for a library implementor which clock is best suited for the developer
 * using the library.
 *
 * std::chrono::time_point is relative to the selected clock's epoch - and as
 * such, time_point is not in itself a clock-agnostic value. That makes it
 * difficult to use time_point for its intended purpose, to specify a point in
 * time, without also knowing which clock to use.
 *
 * We can repurpose the clock-independent std::chrono::duration type, which is
 * technically incorrect. But considering that two events at different times of
 * *the same* clock are some duration apart from each other, we can make things
 * work by just considering this relative duration.
 *
 * With that said, we define a relative_time_point here, which is a high
 * resolution duration, and use this for our purposes.
 */
using relative_time_point = std::chrono::nanoseconds;


/**
 * We also need to use a standard duration, e.g. for specifying timeout
 * values.
 */
using duration = std::chrono::nanoseconds;


/**
 * Now in order to make the relative_time_point usable with a clock, we also
 * provide a simple extraction function that works for multiple chrono clocks.
 */
template <typename clockT>
inline relative_time_point
relative_now()
{
  return std::chrono::duration_cast<relative_time_point>(
      clockT::now().time_since_epoch()
  );
}

} // namespace jsonrpc

#endif // guard
