/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef JSON_RPC_VISIBILITY_H
#define JSON_RPC_VISIBILITY_H

#if defined(_WIN32) || defined(__CYGWIN__) || defined(__MINGW32__)
  #if defined(JSON_RPC_IS_BUILDING) && JSON_RPC_IS_BUILDING > 0
    #define JSON_RPC_API __declspec(dllexport)
  #else
    #define JSON_RPC_API __declspec(dllimport)
  #endif
  #define JSON_RPC_API_FRIEND JSON_RPC_API
#else // Windows
  #if __GNUC__ >= 4
    #define JSON_RPC_API  [[gnu::visibility("default")]]
  #else
    #define JSON_RPC_API
  #endif // GNU C
  #define JSON_RPC_API_FRIEND
#endif // POSIX

// Private symbols may be exported in debug builds for testing purposes.
#if defined(DEBUG)
  #define JSON_RPC_PRIVATE JSON_RPC_API
#else
  #define JSON_RPC_PRIVATE
#endif

#endif // guard
