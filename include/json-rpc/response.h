/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef JSON_RPC_RESPONSE_H
#define JSON_RPC_RESPONSE_H

#include <json-rpc.h>

#include <json-rpc/error.h>

#include <ostream>

namespace jsonrpc {

/**
 * Much like the request structure the response is very lightweight and
 * largely provides validation and some accessors.
 */
class response
{
public:
  // The ID is mandatory, but may be a string or a number
  json              id;

  // We either have a result, or an error; the error is considered
  // absent if it is !is_valid().
  json              result;
  ::jsonrpc::error  error;

  /**
   * Construction and copying
   */
  inline explicit response(json const & input)
  {
    if (validate_response(input)) {
      id = input["id"];
      if (input.contains("error")) {
        error = ::jsonrpc::error{input["error"]};
      }
      else {
        result = input["result"];
      }
    }
  }

  inline response(json const & _id, json const & _result)
  {
    // In ctor, this creates an array with the _data entry
    id = _id; // cppcheck-suppress useInitializationList
    result = _result; // cppcheck-suppress useInitializationList
  }

  inline response(std::string const & _id, json const & _result)
  {
    // In ctor, this creates an array with the _data entry
    id = _id; // cppcheck-suppress useInitializationList
    result = _result; // cppcheck-suppress useInitializationList
  }

  template <typename T>
  inline response(T const & _id, json const & _result)
  {
    static_assert(std::is_integral<T>::value, "Integral (or string) required.");
    // In ctor, this creates an array with the _data entry
    id = _id; // cppcheck-suppress useInitializationList
    result = _result; // cppcheck-suppress useInitializationList
  }

  inline response(json const & _id, ::jsonrpc::error const & _error)
    : error{_error}
  {
    // In ctor, this creates an array with the _data entry
    id = _id; // cppcheck-suppress useInitializationList
  }

  inline response(std::string const & _id, ::jsonrpc::error const & _error)
    : error{_error}
  {
    // In ctor, this creates an array with the _data entry
    id = _id; // cppcheck-suppress useInitializationList
  }

  template <typename T>
  inline response(T const & _id, ::jsonrpc::error const & _error)
    : error{_error}
  {
    static_assert(std::is_integral<T>::value, "Integral (or string) required.");
    // In ctor, this creates an array with the _data entry
    id = _id; // cppcheck-suppress useInitializationList
  }

  inline response(response const &) = default;
  inline response(response &&) = default;
  inline response & operator=(response const &) = default;

  /**
   * Validate a JSON object as a response
   */
  static inline bool validate_response(json const & input)
  {
    // Must be an object.
    if (!input.is_object()) {
      return false;
    }

    // Must have 3 keys.
    if (3 != input.size()) {
      return false;
    }

    // Must contain a "jsonrpc" key with value "2.0"
    if (!input.contains("jsonrpc")) {
      return false;
    }
    if (input["jsonrpc"].get<std::string>() != "2.0") {
      return false;
    }

    // Must either contain an error object, or a result object, but not both.
    if (input.contains("result")) {
      // No further checks necessary.
    }
    else if (input.contains("error")) {
      // Must be an error object; see elsewhere
      if (!error::validate_error(input["error"])) {
        return false;
      }
    }
    else {
      return false;
    }

    // Must contain an ID. If it does, it must be a string or a number.
    if (input.contains("id")) {
      auto the_id = input["id"];
      if (!the_id.is_string() && !the_id.is_number()) {
        return false;
      }
    }

    // Passed all checks
    return true;
  }

  /**
   * Convenience accessors
   */
  inline bool is_valid() const
  {
    return id.is_number() || id.is_string();
  }

  inline bool is_error() const
  {
    return error.is_valid();
  }

  inline json to_json() const
  {
    auto ret = R"({"jsonrpc":"2.0"})"_json;
    ret["id"] = id;
    if (!result.is_null()) {
      ret["result"] = result;
    }
    if (error.is_valid()) {
      ret["error"] = error.to_json();
    }
    return ret;
  }
};


inline std::ostream &
operator<<(std::ostream & os, response const & resp)
{
  os << resp.to_json();
  return os;
}

} // namespace jsonrpc

#endif // guard
