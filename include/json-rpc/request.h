/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef JSON_RPC_REQUEST_H
#define JSON_RPC_REQUEST_H

#include <json-rpc.h>

#include <ostream>

namespace jsonrpc {

/**
 * The request structure is a very simple structure providing almost direct
 * access to the JSON object used to create it, but it
 * a) adds validation in the creation function/constructor, and
 * b) provides some convenience accessors.
 *
 * Note that we disallow null IDs by default; this behaviour can be toggled.
 * If switched on, null IDs will be treated as notifications as in JSON-RPC
 * 1.0.
 */
class request
{
public:
  // The method is mandatory
  std::string   method;

  // Params must be objects or arrays
  json          params;

  // IDs must be string, numerical or null
  json          id = {};


  /**
   * Construction and copying
   */
  inline explicit request(json const & input, bool allow_null_id = false)
  {
    if (validate_request(input, allow_null_id)) {
      method = input["method"].get<std::string>();
      if (input.contains("params")) {
        params = input["params"];
      }
      if (input.contains("id")) {
        id = input["id"];
      }
    }
  }

  inline explicit request(std::string const & _method,
      json const & _params = {})
    : method{_method}
  {
    // In ctor, this creates an array with the _data entry
    params = _params; // cppcheck-suppress useInitializationList
  }

  inline request(request const &) = default;
  inline request(request &&) = default;
  inline request & operator=(request const &) = default;

  /**
   * Validate a JSON object as a request
   */
  static inline bool validate_request(json const & input,
      bool allow_null_id = false)
  {
    // Must be an object.
    if (!input.is_object()) {
      return false;
    }

    // Must have between 2 and 4 keys.
    auto size = input.size();
    if (2 > size || size > 4) {
      return false;
    }

    // Must contain a "jsonrpc" key with value "2.0"
    if (!input.contains("jsonrpc")) {
      return false;
    }
    if (input["jsonrpc"].get<std::string>() != "2.0") {
      return false;
    }

    // Must contain a method that is a string
    if (!input.contains("method")) {
      return false;
    }
    if (!input["method"].is_string()) {
      return false;
    }

    // May contain parameters, but if it does, they must be object or array.
    if (input.contains("params")) {
      if (!input["params"].is_object() && !input["params"].is_array()) {
        return false;
      }
    }

    // May contain an ID. If it does, it must be a string or a number.
    if (input.contains("id")) {
      auto the_id = input["id"];
      if (the_id.is_string() || the_id.is_number()) {
        // ok
      }
      else if (allow_null_id && the_id.is_null()) {
        // ok
      }
      else {
        return false;
      }
    }

    // Passed all checks
    return true;
  }

  /**
   * Convenience accessors
   */
  inline bool is_request() const
  {
    return is_valid() && !id.is_null();
  }

  inline bool is_notification() const
  {
    return is_valid() && id.is_null();
  }

  inline bool has_params() const
  {
    return !params.is_null();
  }

  inline bool is_valid() const
  {
    return !method.empty();
  }

  inline bool is_internal() const
  {
    return (method.rfind("rpc.", 0) == 0);
  }

  inline json to_json() const
  {
    auto ret = R"({"jsonrpc":"2.0"})"_json;
    ret["method"] = method;
    if (params.is_object() || params.is_array()) {
      ret["params"] = params;
    }
    if (id.is_number() || id.is_string()) {
      ret["id"] = id;
    }
    return ret;
  }
};


inline std::ostream &
operator<<(std::ostream & os, request const & req)
{
  os << req.to_json();
  return os;
}


} // namespace jsonrpc

#endif // guard
