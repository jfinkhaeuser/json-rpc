/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef JSON_RPC_H
#define JSON_RPC_H

#ifndef __cplusplus
#error You are trying to include a C++ only header file
#endif

#if __cplusplus < 201703L
#error You must use a C++17 or later compiler, e.g. by setting -std=c++17.
#endif

#include <memory>

/**
 * Which platform are we on?
 **/
#if !defined(JSON_RPC_PLATFORM_DEFINED)
#  if defined(_WIN32) || defined(__WIN32__) || defined(__WINDOWS__)
#    define JSON_RPC_WIN32
#  else
#    define JSON_RPC_POSIX
#  endif
#  define JSON_RPC_PLATFORM_DEFINED
#endif

#include <json-rpc/visibility.h>
#include <json-rpc/version.h>

/**
 * Decide what to include globally
 **/
#if defined(JSON_RPC_WIN32)
// Include windows.h with minimal definitions
#  ifndef WIN32_LEAN_AND_MEAN
#    define WIN32_LEAN_AND_MEAN
#    define __UNDEF_LEAN_AND_MEAN
#  endif
#  define NOMINMAX
// Unicode builds
#  define UNICODE
#  define _UNICODE
#  include <windows.h>
#  include <WinDef.h>
#  ifdef __UNDEF_LEAN_AND_MEAN
#    undef WIN32_LEAN_AND_MEAN
#    undef __UNDEF_LEAN_AND_MEAN
#  endif
#endif


/**
 * Define the JSON type to use, once, otherwise we'll have the same definition
 * everywhere in the code.
 */
#include <nlohmann/json.hpp>

namespace jsonrpc {

using json = ::nlohmann::json;

} // namespace jsonrpc

#endif // guard
