# Release v0.3.0

* #15: Add method to remove parser from parser map.

* #13: Fix two bugs in the partial buffer container.

* #14: Fix bug in incremental parser with fragmented json.

* #18: Fix bug in incremental parser dealing with invalid tokens.

* Miscellaneous: #17

# Release v0.2.0

* #12: Add asynchronous server-side request processing.

# Release v0.1.5

* Fix minor warnings

# Release v0.1.4

* #11: Add some boilerplate documentation
  * Add Code of Conduct.
  * Flesh out Contributing document.
  * Add towncrier for generating change logs.
