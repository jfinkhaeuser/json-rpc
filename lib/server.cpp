/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#include <json-rpc/server.h>

#include <build-config.h>

#include <sstream>

#if defined(JSON_RPC_ENABLE_ASYNC_SERVER)
#include <thread>
#include <mutex>
#include <condition_variable>

#include <liberate/concurrency/command.h>
#endif

#include <json-rpc/processing/server.h>
#include "parse/incremental_parser.h"

namespace jsonrpc {

struct simple_processor : public server::processor
{
  virtual bool
  register_method(std::string const & name, method code, bool overwrite) override final
  {
    return m_processor.register_method(name, code, overwrite);
  }


  virtual bool
  erase_method(std::string const & name) override final
  {
    return m_processor.erase(name);
  }


  virtual void
  invoke(io::peer_tag const & tag, request const & req, result_func result) override final
  {
    auto resp_opt = m_processor.invoke(req);

    // It's possible that the server processed a notification, or rejected
    // the request. If there is a response, however, we need to serialize and
    // return it.
    if (!resp_opt.has_value()) {
      log::logger(m_logfunc) << "No response from method; may be notification.";
      return;
    }

    auto resp = resp_opt.value();
    result(tag, resp);
  }


  virtual void on_log_output(log::function logfunc)
  {
    m_logfunc = logfunc;
  }

  processing::server  m_processor = {};
  log::function       m_logfunc = {};
};


struct server::server_impl
{
  inline ~server_impl()
  {
    if (m_proc_owned) {
      delete m_proc;
      m_proc = nullptr;
    }
  }


  inline void
  consume(io::peer_tag const & tag, void const * buf, size_t size)
  {
    // In order to not mix up data from various peers and still provide
    // incremental parsing, we need an incremental parser per peer tag. Let's
    // create them lazily here.
    auto iter = m_parsers.find(tag);
    if (iter == m_parsers.end()) {
      parser_map::value_type val{tag, parser_type{
          [this, tag](jsonrpc::json const & value)
          {
            // Handle each request
            handle_request(tag, value);
          }
        }
      };

      std::tie(iter, std::ignore) = m_parsers.insert(val);
    }

    // Consume the data incrementally. Not every call may result in a single
    // request handled, or even any, but the buffering in the incremental
    // parser will make sure it succeeds eventually.
    iter->second.push(static_cast<char const *>(buf),
        static_cast<char const *>(buf) + size);
  }

  inline void
  flush_peer(io::peer_tag const & tag)
  {
    // Removes the parser of the given tag from the parser map
    m_parsers.erase(tag);
  }


  inline void
  handle_request(io::peer_tag const & tag, json const & value)
  {
    // First make sure that the request can be created as a request object.
    // This already validates the basic structure. We must allow null IDs for
    // notifications here.
    request req{value, true};

    using namespace std::placeholders;
    m_proc->invoke(tag, req, std::bind(&server_impl::produce, this, _1, _2));
  }



  inline void produce(io::peer_tag const & tag, response const & resp)
  {
    std::stringstream sstream;
    sstream << resp;
    auto buf = sstream.str();

    // Log error responses, just for the record.
    if (resp.is_error()) {
      log::logger(m_logfunc) << "Error response: " << resp;
    }

    m_io_write(tag, buf.c_str(), buf.size());
  }


  using parser_type = parse::incremental_parser<char>;
  using parser_map = std::map<io::peer_tag, parser_type>;
  parser_map                                              m_parsers = {};

  io::write_void_buf                                      m_io_write = {};

  log::function                                           m_logfunc = {};

  server::processor *                                     m_proc = nullptr;
  bool                                                    m_proc_owned = false;
};


server::server(io::write_void_buf func)
  : m_impl{std::make_shared<server_impl>()}
{
  m_impl->m_io_write = func;
  m_impl->m_proc_owned = true;
  m_impl->m_proc = new simple_processor();
}


server::server(io::write_void_buf func, processor & proc)
  : m_impl{std::make_shared<server_impl>()}
{
  m_impl->m_io_write = func;
  m_impl->m_proc = &proc;
  m_impl->m_proc_owned = false;
}




bool
server::register_method(std::string const & name, method code,
    bool overwrite /* = false */)
{
  return m_impl->m_proc->register_method(name, code, overwrite);
}



bool
server::erase_method(std::string const & name)
{
  return m_impl->m_proc->erase_method(name);
}



server &
server::on_log_output(log::function logfunc)
{
  m_impl->m_logfunc = logfunc;
  m_impl->m_proc->on_log_output(logfunc);
  return *this;
}



void
server::consume(io::peer_tag const & tag, void const * buf, size_t size)
{
  m_impl->consume(tag, buf, size);
}


void
server::flush_peer(io::peer_tag const & tag)
{
  m_impl->flush_peer(tag);
}


void
server::produce(io::peer_tag const & tag, response const & res)
{
  m_impl->produce(tag, res);
}


/**
 * async_server_processor
 */
#if defined(JSON_RPC_ENABLE_ASYNC_SERVER)

struct async_server_processor::impl
{
  impl()
    : m_runner{std::bind(&impl::run, this)}
    , m_queue{
        [this](liberate::concurrency::command::concurrent_command_queue &)
        {
          m_command_condition.notify_one();
        },
        [this](liberate::concurrency::command::concurrent_command_queue &)
        {
          m_result_condition.notify_one();
        }
      }
  {
  }


  ~impl()
  {
    m_keep_running = false;
    m_command_condition.notify_all();
    m_result_condition.notify_all();
    m_runner.join();
  }


  void run()
  {
    std::unique_lock lock{m_mutex};
    while (m_keep_running) {
      m_command_condition.wait(lock);
      if (!m_keep_running) {
        break;
      }

      lock.unlock();

      liberate::concurrency::command::concurrent_command_queue::command_ptr cmd;
      while (cmd = m_queue.dequeue_command()) {

        auto the_command = reinterpret_cast<command *>(cmd.get());

        auto resp_opt = m_processor.invoke(the_command->parameters->req);
        if (!resp_opt.has_value()) {
          log::logger(m_logfunc) << "No response from method; may be notification.";
        }
        else {
          the_command->results = std::make_unique<response>(resp_opt.value());
          m_queue.put_results(std::move(cmd));
        }
      }

      lock.lock();
    }
  }


  inline bool register_method(std::string const & name, method code, bool overwrite = false)
  {
    std::unique_lock lock{m_mutex};
    return m_processor.register_method(name, code, overwrite);
  }


  inline bool erase_method(std::string const & name)
  {
    std::unique_lock lock{m_mutex};
    return m_processor.erase(name);
  }

  inline void invoke(io::peer_tag const & tag, request const & req, result_func result)
  {
    auto cmd = liberate::concurrency::command::create_context<command>(0, tag, req, result);
    m_queue.enqueue_command(std::move(cmd));
  }


  inline void on_log_output(log::function logfunc)
  {
    m_logfunc = logfunc;
  }

  inline std::size_t poll_results(std::chrono::milliseconds const & wait_for)
  {
    std::unique_lock lock{m_mutex};
    m_result_condition.wait_for(lock, wait_for);

    liberate::concurrency::command::concurrent_command_queue::command_ptr res;
    std::size_t amount = 0;
    while (res = m_queue.get_completed()) {
      // Due to the way the run() function above is implemented, results will
      // always have a value.
      auto the_result = reinterpret_cast<command *>(res.get());
      the_result->parameters->res(
          the_result->parameters->tag,
          *(the_result->results)
      );

      ++amount;
    }

    return amount;
  }

  // Command parameters here are the peer tag, the request body, and the result
  // function.
  struct params
  {
    inline params(io::peer_tag const & _tag, request const & _req, result_func _res)
      : tag{_tag}
      , req{_req}
      , res{_res}
    {
    }

    io::peer_tag  tag;
    request       req;
    result_func   res;
  };

  using command = liberate::concurrency::command::command_context<params, response>;

  std::mutex              m_mutex = {};
  std::condition_variable m_command_condition = {};
  std::condition_variable m_result_condition = {};

  std::thread             m_runner;
  std::atomic<bool>       m_keep_running = true;

  liberate::concurrency::command::concurrent_command_queue m_queue;

  processing::server      m_processor = {};
  log::function           m_logfunc = {};
};
#endif // defined(JSON_RPC_ENABLE_ASYNC_SERVER)



async_server_processor::async_server_processor()
#if defined(JSON_RPC_ENABLE_ASYNC_SERVER)
  : m_impl{std::make_shared<impl>()}
{
}
#else
{
  throw error{ERR_INTERNAL, "Command queue feature is disabled!"};
}
#endif // defined(JSON_RPC_ENABLE_ASYNC_SERVER)


bool
async_server_processor::register_method(std::string const & name [[maybe_unused]],
    method code [[maybe_unused]], bool overwrite /* = false */ [[maybe_unused]])
{
#if defined(JSON_RPC_ENABLE_ASYNC_SERVER)
  return m_impl->register_method(name, code, overwrite);
#else
  return false;
#endif
}


bool
async_server_processor::erase_method(std::string const & name [[maybe_unused]])
{
#if defined(JSON_RPC_ENABLE_ASYNC_SERVER)
  return m_impl->erase_method(name);
#else
  return false;
#endif
}


void
async_server_processor::invoke(io::peer_tag const & tag [[maybe_unused]],
    request const & req [[maybe_unused]], result_func result [[maybe_unused]])
{
#if defined(JSON_RPC_ENABLE_ASYNC_SERVER)
  m_impl->invoke(tag, req, result);
#endif
}


void
async_server_processor::on_log_output(log::function logfunc [[maybe_unused]])
{
#if defined(JSON_RPC_ENABLE_ASYNC_SERVER)
  m_impl->on_log_output(logfunc);
#endif
}



std::size_t
async_server_processor::poll_results(
    std::chrono::milliseconds const & wait_for /* = {} */ [[maybe_unused]])
{
#if defined(JSON_RPC_ENABLE_ASYNC_SERVER)
  return m_impl->poll_results(wait_for);
#else
  return 0;
#endif
}




} // namespace jsonrpc
