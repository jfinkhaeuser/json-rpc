/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#include <json-rpc/client.h>

#include <sstream>

#include <json-rpc/processing/client.h>
#include "parse/incremental_parser.h"

namespace jsonrpc {

struct client::client_impl
{
  inline explicit client_impl(duration const & timeout)
    : m_processor{timeout}
  {
  }



  inline void
  send_request(io::peer_tag const & tag, std::string const & method_name,
      json const & params, response_callback cb,
      relative_time_point const & now)
  {
    auto req = m_processor.create_request(method_name, params, now, tag);

    // If there is a callback, we'll also have a specific id to remember it by.
    if (cb) {
      m_request_cbs[req.id] = cb;
    }

    dispatch(tag, req);
  }



  inline void
  send_notification(io::peer_tag const & tag, std::string const & method,
        json const & params)
  {
    auto req = m_processor.create_notification(method, params);
    dispatch(tag, req);
  }



  inline void
  dispatch(io::peer_tag const & tag, request const & req)
  {
    std::stringstream sstream;
    sstream << req;
    std::string buf = sstream.str();

    m_io_write(tag, buf.c_str(), buf.size());
  }


  inline void
  consume(io::peer_tag const & tag, void const * buf, size_t size,
      relative_time_point const & now)
  {
    // In order to not mix up data from various peers and still provide
    // incremental parsing, we need an incremental parser per peer tag. Let's
    // create them lazily here.
    auto iter = m_parsers.find(tag);
    if (iter == m_parsers.end()) {
      parser_map::value_type val{tag, parser_type{
          [this, tag](jsonrpc::json const & value,
              relative_time_point const & _now)
          {
            // The callback pushes each response bound to this parser's tag to
            // handle_response() below.
            handle_response(tag, value, _now);
          }
        }
      };

      std::tie(iter, std::ignore) = m_parsers.insert(val);
    }

    // Consume the data incrementally. Not every call may result in a single
    // response handled, or even any, but the buffering in the incremental
    // parser will make sure it succeeds eventually.
    iter->second.push(static_cast<char const *>(buf),
        static_cast<char const *>(buf) + size,
        now);
  }


  inline void
  handle_response(io::peer_tag const & tag, jsonrpc::json const & value,
      relative_time_point const & now)
  {
    // First thing is, we need to parse the response and see if it's valid.
    response resp{value};
    if (!resp.is_valid()) {
      // Ignore invalid responses completely.
      log::logger(m_logfunc) << "Received response is invalid, ignoring: "
        << value;
      return;
    }

    // Have the processing client receive the response; this provides us with a
    // status.
    auto results = m_processor.receive_response(resp, now, tag);
    process_results(results);
  }



  inline void process_results(std::vector<result<io::peer_tag>> const & results)
  {
    for (auto & [event, opt_resp, tag] : results) {
      // We can unpack the actual response here. It exists, but it may be
      // invalid
      auto & resp = opt_resp.value();

      switch (event) {
        case RE_OK_RESPONSE:
        case RE_ERROR_RESPONSE:
        case RE_TIMEOUT:
          // Continue passing to callbacks.
          break;

        case RE_UNKNOWN_REQUEST_ID:
          log::logger(m_logfunc) << "Unknown request id: " << resp.id;
          // TODO Consider an error callback. See
          //      https://codeberg.org/jfinkhaeuser/json-rpc/issues/7
          return;

        case RE_PROCESSING_ERROR:
          log::logger(m_logfunc) << "Error processing response, discarding.";
          // TODO Consider an error callback. See
          //      https://codeberg.org/jfinkhaeuser/json-rpc/issues/7
          return;
      }

      // If we have a response callback registered for this particular request
      // ID, we need to invoke it - but also delete the callback.
      auto id_iter = m_request_cbs.find(resp.id.get<id_type>());
      if (id_iter != m_request_cbs.end()) {
        auto cb = id_iter->second;
        m_request_cbs.erase(id_iter);
        cb(tag, event, opt_resp);
        return;
      }

      // If we have a tagged callback, we need to invoke that instead. This is
      // not something we need to delete.
      auto tag_iter = m_tagged_cbs.find(tag);
      if (tag_iter != m_tagged_cbs.end()) {
        tag_iter->second(tag, event, opt_resp);
        return;
      }

      // Finally, if we have a generic callback, invoke that.
      if (m_generic_cb) {
        m_generic_cb(tag, event, opt_resp);
        return;
      }

      // No callbacks are interested in this response, discarding it silently.
      log::logger(m_logfunc) << "Got valid response, but no callback is "
        "interested in it, discarding: " << resp;
    }
  }


  inline void process_timeouts(relative_time_point const & now)
  {
    auto results = m_processor.process_timeouts(now);
    process_results(results);
  }



  processing::client<io::peer_tag>                        m_processor;

  client::response_callback                               m_generic_cb = {};
  std::map<io::peer_tag, client::response_callback>       m_tagged_cbs = {};

  using id_type = typename processing::client<io::peer_tag>::id_type;
  std::map<id_type, client::response_callback>           m_request_cbs = {};

  using parser_type = parse::incremental_parser<
    char,
    parse::detail::array_buffer<char>,
    relative_time_point
  >;
  using parser_map = std::map<io::peer_tag, parser_type>;
  parser_map                                              m_parsers = {};

  io::write_void_buf                                      m_io_write = {};

  log::function                                           m_logfunc = {};
};



client::client(io::write_void_buf func, duration const & timeout /* = max() */)
  : m_impl{std::make_shared<client_impl>(timeout)}
{
  m_impl->m_io_write = func;
}



client &
client::on_response(response_callback cb)
{
  m_impl->m_generic_cb = cb;
  return *this;
}



client &
client::on_response(io::peer_tag const & tag, response_callback cb)
{
  m_impl->m_tagged_cbs[tag] = cb;
  return *this;
}



client &
client::on_log_output(log::function logfunc)
{
  m_impl->m_logfunc = logfunc;
  return *this;
}



void
client::consume(io::peer_tag const & tag, void const * buf, size_t size,
    relative_time_point const & now)
{
  m_impl->consume(tag, buf, size, now);
}



void
client::send_request(io::peer_tag const & tag, std::string const & method_name,
    json const & params /* = {} */, response_callback cb /* = {} */,
    relative_time_point const & now /* = {} */)
{
  m_impl->send_request(tag, method_name, params, cb, now);
}



void
client::send_notification(io::peer_tag const & tag,
    std::string const & method_name,
    json const & params /* = {} */)
{
  m_impl->send_notification(tag, method_name, params);
}



void
client::process_timeouts(relative_time_point const & now)
{
  m_impl->process_timeouts(now);
}


} // namespace jsonrpc
