/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2020-2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef JSON_RPC_PARSE_INCREMENTAL_PARSER_H
#define JSON_RPC_PARSE_INCREMENTAL_PARSER_H

#include <json-rpc.h>

#include <functional>

#include "partial_buffer_container.h"

namespace jsonrpc::parse {

/**
 * The incremental_parser expects to have raw data fed to it in chunks, that
 * contains a sequence of JSON arrays or objects, without any delimiters or
 * other markers.
 *
 * This abstraction helps parse individual JSON-RPC requests or responses from
 * either a Byte stream or a sequence of datagrams. In the latter case, each
 * chunk would typically contain a single JSON request or response object.
 *
 * Since it's going to be used in an I/O context, it's the network that decides
 * when data arrives, and the parser has to react accordingly. As a consequence,
 * the design is that of a push parser: one pushes new data chunks into the
 * parser and expects callbacks for each JSON entity.
 *
 * This can be used directly with I/O loops for e.g. unix domain, TCP or UDP
 * sockets. HTTP-based I/O takes somw (un)wrapping first.
 *
 * Note: the parser accepts any kind of char, but single-byte chars are expected
 *       to be encoded as UTF-8 byte sequences.
 */
template <
  typename charT,
  typename bufferT = detail::array_buffer<charT>,
  typename batonT = void *
>
struct incremental_parser
{
public:
  using callback = std::function<void (json const &, batonT const &)>;
  using batonless_callback = std::function<void (json const &)>;

  inline explicit incremental_parser(callback cb)
    : m_callback{cb}
  {
  }


  inline explicit incremental_parser(batonless_callback cb)
    : m_callback{
        [cb](json const & val, batonT const &)
        {
          cb(val);
        }
      }
  {
  }


  inline void push(charT const * range_begin, charT const * range_end,
      batonT const & baton = batonT{})
  {
    // We need to keep what's in the container, and add the current range.
    // There might be nothing in the container, in which case nothing will
    // be kept.
    m_container.continue_with(0, range_begin, range_end);

    // Create a callback for the JSON parser. Every end of object or array
    // event at the top level, it invokes the callback.
    json::parser_callback_t cb = [this, &baton](int depth, json::parse_event_t event, json & parsed) -> bool
    {
      // Track current depth
      m_current_depth = depth;

      // If we've just closed an element at depth 0, we can report it finished.
      if (depth == 0 && (event == json::parse_event_t::object_end || event == json::parse_event_t::array_end)) {
        m_callback(parsed, baton);
      }
      return true;
    };

    // Try passing the container to the json parsing function.
    while (m_container.size() > 0) {
      try {
        auto res [[maybe_unused]] = json::parse(m_container, cb, true);
        // Finished parsing, break out of the loop. But first, make sure that
        // whatever is consumed of the buffer is truncated.
        m_container.truncate_front(m_container.consume_offset());
        break;
      } catch (json::parse_error const & ex) {
        // Parse errors have an ID, and 101 stands for an unexpected token.
        if (ex.id == 101) {
          // However, we can track the current depth - and if that happens to
          // be zero, it looks like we're just starting a new element in the
          // stream and can continue from there.
          if (m_current_depth == 0) {
            //if first char caused the exception, remove it
            if (ex.byte <= 1) {
              m_container.truncate_front(1);
            }
            else {
              m_container.truncate_front(ex.byte - 1);
            }
          }
          else {
            // If the error occurred at the byte offset marking the end of the
            // container, it's clear that some more input is needed.
            if (ex.byte == m_container.size() + 1) {
             m_container.copy_remaining(0);
              break;
            }
            else {
              // Otherwise this looks like garbage input.
              throw;
            }
          }
        }
        else {
          // All other IDs we should re-throw, just as any other parse error.
          throw;
        }
      }
    }
  }


private:
  callback  m_callback;
  int       m_current_depth = 0;

  using pbc = partial_buffer_container<charT, bufferT>;
  pbc       m_container = {};

};


} // namespace jsonrpc::parse

#endif // guard
