/**
 * This file is part of json-rpc.
 *
 * Author(s): Jens Finkhaeuser <jens@finkhaeuser.de>
 *
 * Copyright (c) 2021 Jens Finkhaeuser.
 *
 * This software is licensed under the terms of the GNU GPLv3 for personal,
 * educational and non-profit use. For all other uses, alternative license
 * options are available. Please contact the copyright holder for additional
 * information, stating your intended usage.
 *
 * You can find the full text of the GPLv3 in the COPYING file in this code
 * distribution.
 *
 * This software is distributed on an "AS IS" BASIS, WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.
 **/
#ifndef JSON_RPC_PARSE_PARTIAL_BUFFER_CONTAINER_H
#define JSON_RPC_PARSE_PARTIAL_BUFFER_CONTAINER_H

#include <iterator>
#include <vector>
#include <cstring>
#include <cstddef>

namespace jsonrpc::parse {

/**
 * The partial_buffer_container helper class provides a container-like
 * interface with iterators around a raw input type buffer, usually a type of
 * char or byte, with one addtional piece of functionality.
 *
 * When instructed to do so, the container will copy the remaining input buffer
 * from a given offset, and wrap a new input buffer. When the container is then
 * iterated over, the first elements will be taken from the copied remains
 * before proceeding to the new buffer. In this fashion, the container easily
 * allows partial buffering of unprocessed data, whilst generally operating in
 * a non-copying manner.
 *
 * The exact buffering behaviour is factored out into a buffer class, which
 * allows for fixed size as well as variable sized buffers (via vector).
 */


namespace detail {

template <typename charT>
struct vector_buffer
{
  using vec = std::vector<charT>;

  /*
   * Move all remaining bytes from the given offset to the front of the buffer. Return the
   * new buffer size (i.e. the remaining bytes).
   */
  inline std::size_t move_to_front(std::size_t offset)
  {
    if (offset > size()) {
      throw std::logic_error("Offset cannot exceed size.");
    }

    auto amount = size() - offset;
    if (!amount) {
      m_buf.resize(amount);
      return 0;
    }

    std::memmove(&m_buf[0], &m_buf[offset], amount);
    m_buf.resize(amount);

    return amount;
  }

  inline std::size_t copy(std::size_t offset, charT const * begin, std::size_t size)
  {
    std::size_t new_size = offset + size;
    m_buf.resize(new_size);
    std::memcpy(&m_buf[offset], begin, size); // flawfinder: ignore
    return size;
  }

  inline std::size_t size() const
  {
    return m_buf.size();
  }

  inline charT get(std::size_t offset) const
  {
    return m_buf[offset];
  }

  vec m_buf = {};
};


template <
  typename charT,
  std::size_t SIZE = 32768
>
struct array_buffer
{
  /*
   * Move all remaining bytes from the given offset to the front of the buffer. Return the
   * new buffer size (i.e. the remaining bytes).
   */
  inline std::size_t move_to_front(std::size_t offset)
  {
    if (offset > size()) {
      throw std::logic_error("Offset cannot exceed size.");
    }

    m_used = size() - offset;
    if (m_used) {
      std::memmove(&m_buf[0], &m_buf[offset], m_used);
    }
    return m_used;
  }

  inline std::size_t copy(std::size_t offset, charT const * begin, std::size_t size)
  {
    std::size_t new_size = offset + size;
    if (new_size > SIZE) {
      throw std::runtime_error("Buffer too small");
    }
    std::memcpy(&m_buf[offset], begin, size); // flawfinder: ignore
    m_used = new_size;
    return size;
  }

  inline std::size_t size() const
  {
    return m_used;
  }

  inline charT get(std::size_t offset) const
  {
    return m_buf[offset];
  }

  std::size_t m_used = 0;
  charT       m_buf[SIZE];
};




} // namespace detail


/**
 * The partial_buffer_container class acts as the aforementioned container.
 *
 * The new functionality is in the copy_remaining(), set_input() and
 * continue_with() functions. Finally, there's a truncate_front() function
 * that strips all content preceeding the given offset or iterator.
 */
template <
  typename charT,
  typename bufferT = detail::array_buffer<charT>
>
struct partial_buffer_container
{
  /**
   * We define a simple iterator and its const version. It contains a reference
   * to its container and an offset.
   */
  struct iterator : public std::iterator<
    std::input_iterator_tag,  // iterator_category
    charT,                    // value_type
    std::ptrdiff_t,           // difference_type
    charT const *,            // pointer
    charT const &             // reference
  >
  {
    inline charT operator*() const
    {
      return m_pbc->get(m_offset);
    }

    inline iterator & operator++()
    {
      ++m_offset;
      if (m_offset >= m_pbc->size()) {
        m_offset = max_offset();
      }
      m_pbc->m_consume_offset = m_offset;
      return *this;
    }

    inline iterator operator++(int) // postfix
    {
      iterator ret{*this};
      ++ret;
      return ret;
    }

    inline std::ptrdiff_t operator-(iterator const & other) const
    {
      auto offset = m_offset;
      if (m_offset == std::numeric_limits<std::size_t>::max()) {
        offset = m_pbc->size();
      }
      return offset - other.m_offset;
    }

    inline bool operator==(iterator const & other) const
    {
      return (m_pbc == other.m_pbc && m_offset == other.m_offset);
    }

    inline bool operator!=(iterator const & other) const
    {
      return (m_pbc != other.m_pbc || m_offset != other.m_offset);
    }

    inline iterator(iterator const &) = default;
    inline iterator(iterator &&) = default;
    inline iterator & operator=(iterator const &) = default;

  private:
    friend struct partial_buffer_container;

    inline iterator(partial_buffer_container * pbc, std::size_t offset)
      : m_pbc(pbc)
      , m_offset{offset >= pbc->size()
        ? std::numeric_limits<std::size_t>::max()
        : offset
      }
    {
    }

    partial_buffer_container *  m_pbc;
    std::size_t                 m_offset;
  };

  using const_iterator = iterator;

  /**
   * The constructor takes an input range.
   */
  inline partial_buffer_container(charT const * range_begin, charT const * range_end)
    : m_begin{range_begin}
    , m_end{range_end}
  {
  }

  inline partial_buffer_container()
    : m_begin{nullptr}
    , m_end{nullptr}
  {
  }

  /**
   * Create iterators
   */
  inline iterator begin()
  {
    return {this, 0};
  }

  inline const_iterator begin() const
  {
    return {const_cast<partial_buffer_container<charT, bufferT>*>(this), 0};
  }

  inline iterator end()
  {
    return {this, max_offset()};
  }

  inline const_iterator end() const
  {
    return {const_cast<partial_buffer_container<charT, bufferT>*>(this),
      max_offset()};
  }

  inline std::size_t size() const
  {
    return m_buffer.size() + input_size();
  }

  inline std::size_t consume_offset() const
  {
    return m_consume_offset;
  }

  inline static constexpr std::size_t max_offset()
  {
    return std::numeric_limits<std::size_t>::max();
  }

  /**
   * The function copies the remaining input buffer from the offset until the
   * input end into the internal buffer; the class also forgets about the input
   * buffer at this point. This means that any existing iterators will be
   * invalidated.
   *
   * New iterator will return values from the internal buffer until its end is
   * reached, at which point the input buffer would be consulted again (which
   * is unset). To set a new input buffer, use the set_input() function below.
   */
  inline std::size_t copy_remaining(std::size_t offset)
  {
    // Special case for "truncate everything", no copying necessary.
    if (offset == max_offset()) {
      m_begin = m_end;
      m_buffer.move_to_front(m_buffer.size());
      return m_buffer.size();
    }

    if (offset > size()) {
      throw std::runtime_error("Invalid offset beyond end of input.");
    }

    std::size_t copy_offset = 0;

    // From buffer
    if (offset < m_buffer.size()) {
      copy_offset += m_buffer.move_to_front(offset);
      offset = 0;
    }
    else {
      offset -= m_buffer.size();
    }

    // From input
    auto in_size = m_end - m_begin;
    size_t from_input = in_size - offset;
    m_buffer.copy(copy_offset, m_begin + offset, from_input);

    // If we copy the remaining buffer, we need to also reset the input.
    m_begin = m_end = nullptr;

    // Success!
    return m_buffer.size();
  }

  inline std::size_t copy_remaining(iterator const & iter)
  {
    return copy_remaining(iter.m_offset);
  }


  /**
   * Sets a new input buffer ranges. This invalidates any existing iterators,
   * or cause unwanted side-effects.
   */
  inline void set_input(charT const * range_begin, charT const * range_end)
  {
    m_begin = range_begin;
    m_end = range_end;
  }


  /**
   * This function provides a convenience interface for relatively continuous
   * procesisng of chunked input, as from e.g. an I/O subsystem:
   *
   * partial_buffer_container pbc{initial_start, initial_end};
   *
   * auto iter = pbc.begin();
   * for (; iter != pbc.end() ; ++iter) {
   *   // Process *iter until e.g. new input is available.
   *   iter = pbc.continue_with(iter, new_start, new_end);
   * }
   *
   * One side-effect of this usage is that if new input is available faster
   * than iteration proceeds, all that means is that the internal buffer grows
   * accordingly.
   */
  inline iterator continue_with(std::size_t offset, charT const * range_begin,
      charT const * range_end)
  {
    copy_remaining(offset);
    set_input(range_begin, range_end);
    return begin();
  }


  inline iterator continue_with(iterator const & iter, charT const * range_begin,
      charT const * range_end)
  {
    return continue_with(iter.m_offset, range_begin, range_end);
  }


  /**
   * The truncate_front() function strips all content preceeding the given
   * offset. This is a cheap iteration if the entire buffer is discarded, but
   * if the internal buffer is only partially discarded, it may be a little more costly.
   */
  inline void truncate_front(std::size_t offset)
  {
    // Special case for "truncate everything"
    if (offset == max_offset()) {
      m_begin = m_end;
      m_buffer.move_to_front(m_buffer.size());
      return;
    }

    if (offset > size()) {
      throw std::runtime_error("Invalid offset beyond end of input.");
    }

    // Calculate amount to truncate from internal buffer.
    auto from_buffer = std::min(m_buffer.size(), offset);
    if (from_buffer > 0) {
      m_buffer.move_to_front(from_buffer);
      offset -= from_buffer;
    }

    // If the remaining offset is just part of the input buffer, we can just
    // advance the start pointer.
    m_begin += offset;
  }

  inline void truncate_front(iterator const & iter)
  {
    truncate_front(iter.m_offset);
  }

private:
  friend struct iterator;

  inline charT get(std::size_t offset) const
  {
    if (offset < m_buffer.size()) {
      return m_buffer.get(offset);
    }

    auto idx = offset - m_buffer.size();
    if (idx >= input_size()) {
      throw std::runtime_error("Out of bounds");
    }
    return m_begin[idx];
  }

  inline std::size_t input_size() const
  {
    return m_end - m_begin;
  }

  bufferT       m_buffer;

  charT const * m_begin;
  charT const * m_end;

  std::size_t   m_consume_offset = 0;
};


/**
 * This is an unfortunate necessity for nlohmann_json's ADL to work.
 */
using std::begin;
using std::end;


} // namespace jsonrpc::parse

#endif // guard
