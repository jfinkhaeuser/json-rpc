# Contributing

We love pull requests from everyone. By participating in this project, you
agree to abide by the project [code of conduct].

[code of conduct]: https://codeberg.org/jfinkhaeuser/json-rpc/src/branch/main/CODE_OF_CONDUCT.md

Fork, then clone the repo:

    git clone git@github.com:your-username/prance.git

## Setup

Why is there a Pipfile? Well, with meson, you're using Python anyway, so we
might as well also use other tools.

```bash
$ pip install pipenv
```

Installing pipenv lets you use the `Pipfile` to create a local installation
of some Python based tools. We don't actually use many here. However, using
the following will give you a shell with the tools activated. See the `Pipfile`
for details on those tools.

```bash
$ pipenv shell
```

## Changelog

We're using [towncrier](https://pypi.org/project/towncrier/) to generate a
changelog. We don't use custom change types, so pick one of `feature`,
`bugfix`, `doc`, `removal`, `misc`.

Create a simple text file in `changelog.d/<issue-or-pr>.<type>`. Write a
concise summary of the change and how it affects users.

For very small changes, use `misc` - the descriptions from that type won't
even be added to the changelog, just a link ot the issue.

Prefer issue numbers over PR numbers, if you have both.

You can run `towncrier --draft` to see how your changes would appear in
the changelog. Running without draft would alter `CHANGES.rst`, so if you
do that, make sure *not* to commit the result. That'll be done on `master`
during release.

## Pull Requests

Push to your fork and [submit a pull request][pr].

[pr]: https://codeberg.org/jfinkhaeuser/json-rpc/pulls

At this point you're waiting on us. We intend to respond to PRs within a few business days,
but nobody pays us to do so. Please be patient.

We may suggest some changes or improvements or alternatives.

Some things that will increase the chance that your pull request is accepted:

* Write tests.
* Follow our style guide. Running the tests runs flake8, our style guide checker.
* Add a changelog entry to your PR; we can then review the PR together with how
  you would describe it to end users.
* Write a [good commit message][commit].

[commit]: http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html

## Releases

Merge all outstanding PRs. Build and run unit tests. Make sure they all build,
and fix them if they don't succeed. Fixing can mean disabling them and creating
an issue to look into the matter post-release, but that's not generally the
best idea.

Create a changelog with towncrier, making sure to delete the fragment files.

**Important:** towncrier and bump2version both deal with project versions. The
changelog created by towncrier should be part of the tag, though, so it needs
to be created *before* bump2version can do its thing. For this reason, you 
*must* pass the new version via the `--version` option to towncrier.

Commit those changes, i.e. the deleted fragment and updated changelog.

Then, when a version is ready for release, you can use the excellent
[bump2version](https://pypi.org/project/bump2version/) tool to update build
files and tag the release.
